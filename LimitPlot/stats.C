#define stats_cxx
#include "stats.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

void stats::Loop()
{
  //   In a ROOT session, you can do:
  //      root> .L stats.C
  //      root> stats t
  //      root> t.GetEntry(12); // Fill t data members with entry number 12
  //      root> t.Show();       // Show values of entry 12
  //      root> t.Show(16);     // Read and show values of entry 16
  //      root> t.Loop();       // Loop on all entries
  //

  //     This is the loop skeleton where:
  //    jentry is the global entry number in the chain
  //    ientry is the entry number in the current Tree
  //  Note that the argument to GetEntry must be:
  //    jentry for TChain::GetEntry
  //    ientry for TTree::GetEntry and TBranch::GetEntry
  //
  //       To read only selected branches, Insert statements like:
  // METHOD1:
  //    fChain->SetBranchStatus("*",0);  // disable all branches
  //    fChain->SetBranchStatus("branchname",1);  // activate branchname
  // METHOD2: replace line
  //    fChain->GetEntry(jentry);       //read all branches
  //by  b_branchname->GetEntry(ientry); //read only this branch
  if (fChain == 0) return;

  TH1F *h = new TH1F("limit","limit",8,0,8);

  Long64_t nentries = fChain->GetEntriesFast();

  Long64_t nbytes = 0, nb = 0;
  for (Long64_t jentry=0; jentry<nentries;jentry++) {
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0) break;
    nb = fChain->GetEntry(jentry);   nbytes += nb;
    // if (Cut(ientry) < 0) continue;

    int mass = std::stoi("1000");
    cout << "mass = " << mass;
    cout << " exp_upperlimit = " << exp_upperlimit;
    cout << " exp_upperlimit_plus1 = " << exp_upperlimit_plus1;
    cout << " exp_upperlimit_plus2 = " << exp_upperlimit_plus2;
    cout << " exp_upperlimit_minus1 = " << exp_upperlimit_minus1;
    cout << " exp_upperlimit_minus2 = " << exp_upperlimit_minus2;
    cout << endl;

    // fill to histogram
    h->SetBinContent(1,exp_upperlimit); // Observed (blinded now)
    h->SetBinContent(2,exp_upperlimit); // Expected
    h->SetBinContent(3,exp_upperlimit_plus2);
    h->SetBinContent(4,exp_upperlimit_plus1);
    h->SetBinContent(5,exp_upperlimit_minus1);
    h->SetBinContent(6,exp_upperlimit_minus2);
    h->SetBinContent(7,0); // Reserved. Not used now
    h->SetBinContent(8,0); // Reserved. Not used now

  }

  // output
  TFile outfile("./input_for_plot/Zp_1000.root","recreate");
  h->Write(0, TObject::kOverwrite);

  outfile.Write();
  outfile.Close();
}
