//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sun Dec  1 15:26:39 2019 by ROOT version 6.04/16
// from TTree stats/runAsymptoticsCLs
// found on file: myLimit_CL95.root
//////////////////////////////////////////////////////////

#ifndef stats_h
#define stats_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class stats {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Float_t         parameter;
   Float_t         CLb_med;
   Float_t         pb_med;
   Float_t         CLs_med;
   Float_t         CLsplusb_med;
   Float_t         CLb_obs;
   Float_t         pb_obs;
   Float_t         CLs_obs;
   Float_t         CLsplusb_obs;
   Float_t         obs_upperlimit;
   Float_t         inj_upperlimit;
   Float_t         exp_upperlimit;
   Float_t         exp_upperlimit_plus1;
   Float_t         exp_upperlimit_plus2;
   Float_t         exp_upperlimit_minus1;
   Float_t         exp_upperlimit_minus2;
   Float_t         fit_status;
   Float_t         mu_hat_obs;
   Float_t         mu_hat_exp;
   Float_t         param_Muttbar_hat;
   Float_t         param_alpha_LUMI_hat;
   Float_t         param_Muttbar_med;
   Float_t         param_alpha_LUMI_med;

   // List of branches
   TBranch        *b_parameter;   //!
   TBranch        *b_CLb_med;   //!
   TBranch        *b_pb_med;   //!
   TBranch        *b_CLs_med;   //!
   TBranch        *b_CLsplusb_med;   //!
   TBranch        *b_CLb_obs;   //!
   TBranch        *b_pb_obs;   //!
   TBranch        *b_CLs_obs;   //!
   TBranch        *b_CLsplusb_obs;   //!
   TBranch        *b_obs_upperlimit;   //!
   TBranch        *b_inj_upperlimit;   //!
   TBranch        *b_exp_upperlimit;   //!
   TBranch        *b_exp_upperlimit_plus1;   //!
   TBranch        *b_exp_upperlimit_plus2;   //!
   TBranch        *b_exp_upperlimit_minus1;   //!
   TBranch        *b_exp_upperlimit_minus2;   //!
   TBranch        *b_fit_status;   //!
   TBranch        *b_mu_hat_obs;   //!
   TBranch        *b_mu_hat_exp;   //!
   TBranch        *b_param_Muttbar_hat;   //!
   TBranch        *b_param_alpha_LUMI_hat;   //!
   TBranch        *b_param_Muttbar_med;   //!
   TBranch        *b_param_alpha_LUMI_med;   //!

   stats(TTree *tree=0);
   virtual ~stats();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef stats_cxx
stats::stats(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("input/di_higgs_pDNN_fit_1000/Limits/asymptotics/myLimit_CL95.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("input/di_higgs_pDNN_fit_1000/Limits/asymptotics/myLimit_CL95.root");
      }
      f->GetObject("stats",tree);

   }
   Init(tree);
}

stats::~stats()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t stats::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t stats::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void stats::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("parameter", &parameter, &b_parameter);
   fChain->SetBranchAddress("CLb_med", &CLb_med, &b_CLb_med);
   fChain->SetBranchAddress("pb_med", &pb_med, &b_pb_med);
   fChain->SetBranchAddress("CLs_med", &CLs_med, &b_CLs_med);
   fChain->SetBranchAddress("CLsplusb_med", &CLsplusb_med, &b_CLsplusb_med);
   fChain->SetBranchAddress("CLb_obs", &CLb_obs, &b_CLb_obs);
   fChain->SetBranchAddress("pb_obs", &pb_obs, &b_pb_obs);
   fChain->SetBranchAddress("CLs_obs", &CLs_obs, &b_CLs_obs);
   fChain->SetBranchAddress("CLsplusb_obs", &CLsplusb_obs, &b_CLsplusb_obs);
   fChain->SetBranchAddress("obs_upperlimit", &obs_upperlimit, &b_obs_upperlimit);
   fChain->SetBranchAddress("inj_upperlimit", &inj_upperlimit, &b_inj_upperlimit);
   fChain->SetBranchAddress("exp_upperlimit", &exp_upperlimit, &b_exp_upperlimit);
   fChain->SetBranchAddress("exp_upperlimit_plus1", &exp_upperlimit_plus1, &b_exp_upperlimit_plus1);
   fChain->SetBranchAddress("exp_upperlimit_plus2", &exp_upperlimit_plus2, &b_exp_upperlimit_plus2);
   fChain->SetBranchAddress("exp_upperlimit_minus1", &exp_upperlimit_minus1, &b_exp_upperlimit_minus1);
   fChain->SetBranchAddress("exp_upperlimit_minus2", &exp_upperlimit_minus2, &b_exp_upperlimit_minus2);
   fChain->SetBranchAddress("fit_status", &fit_status, &b_fit_status);
   fChain->SetBranchAddress("mu_hat_obs", &mu_hat_obs, &b_mu_hat_obs);
   fChain->SetBranchAddress("mu_hat_exp", &mu_hat_exp, &b_mu_hat_exp);
   fChain->SetBranchAddress("param_Muttbar_hat", &param_Muttbar_hat, &b_param_Muttbar_hat);
   fChain->SetBranchAddress("param_alpha_LUMI_hat", &param_alpha_LUMI_hat, &b_param_alpha_LUMI_hat);
   fChain->SetBranchAddress("param_Muttbar_med", &param_Muttbar_med, &b_param_Muttbar_med);
   fChain->SetBranchAddress("param_alpha_LUMI_med", &param_alpha_LUMI_med, &b_param_alpha_LUMI_med);
   Notify();
}

Bool_t stats::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void stats::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t stats::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef stats_cxx
