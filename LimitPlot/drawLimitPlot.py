#!/usr/bin/env python
import ROOT as R
import glob
import PyROOTUtils
import AtlasStyle, AtlasUtil
R.gROOT.SetBatch()
import sys
from array import array

#tag = "v9_noStat_noSyst_19Jan_oldNorm_ggF"
#tag = "v9_afs_noSyst_unblind_ggF"
#tag = "v9_noStat_noSyst_19Jan_newRhomin_interp3_ggF"
#mode= "ggF"

print 'start of script'
tag = sys.argv[1]
alg_type = sys.argv[2]
channel = sys.argv[3]
yname = sys.argv[4]
ymax = sys.argv[5]
savename = sys.argv[6]
xaxisname = sys.argv[7]
Lumi = 139 # in fb-1

def get_cxs(filename, masses ):
    f_xs = open(filename,'r')
    vals = []
    file_masses = []
    for line in f_xs:
        line = line.strip().split()
        if len(line)<2: print line
        vals .append( float(line[1]) )
        file_masses .append( float(line[0]) )
    cx = [ linear_interp(mH, file_masses, vals) for mH in masses ] 
    return cx

# tag="20161019"
  
print tag, alg_type

#The root file has a 7-bin TH1D named 'limit', where each bin is filled with the upper limit values in this order:
#
#    1: Observed
#    2: Median
#    3: +2 sigma
#    4: +1 sigma
#    5: -1 sigma
#    6: -2 sigma
#    7: mu=0 fit status (only meaningful if asimov data is generated within the macro)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Linear interpolation like numpy.interp
def linear_interp(x, xvals, yvals):
    xvals = [float(v) for v in xvals]
    yvals = [float(v) for v in yvals]
    if x <= xvals[0]: return yvals[0]
    if x >= xvals[-1]: return yvals[-1]
    for iX in range(len(xvals)):
        if x < xvals[iX]:
            break
    # Now iX is the index of xvals just bigger than x
    return yvals[iX-1] + (yvals[iX-1]-yvals[iX])/(xvals[iX-1]-xvals[iX]) * (x-xvals[iX-1])
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Can be used to load CX or BR from text file
# assumue first column in text file is mH, second is CX/BR
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


results = {}

class LimitAtMh:
    def __init__(self, mh, th1, alg_type): 
        self.mH = mH
        if alg_type.find("CLS") != -1:
            self.obs = th1.GetBinContent(1)
            self.med = th1.GetBinContent(2)
            self.plus2sigma = th1.GetBinContent(3)
            self.plus1sigma = th1.GetBinContent(4)
            self.minus1sigma = th1.GetBinContent(5)
            self.minus2sigma = th1.GetBinContent(6)
            self.status = th1.GetBinContent(7)
            self.psplusb = th1.GetBinContent(8)
        elif alg_type == "Hypo":
            self.obs = th1.UpperLimit()
            self.med = th1.GetExpectedUpperLimit(0)
            self.plus2sigma = th1.GetExpectedUpperLimit(2)
            self.plus1sigma = th1.GetExpectedUpperLimit(1)
            self.minus1sigma = th1.GetExpectedUpperLimit(-1)
            self.minus2sigma = th1.GetExpectedUpperLimit(-2)
            self.status = 0
        #print self.mH
        #print self.obs
        #print self.med
        #print self.plus2sigma
        #print self.plus1sigma
        #print self.minus1sigma
        #print self.minus2sigma
        #print self.status

for rfname in glob.glob(tag+"/"+channel+"*.root"):
    mH = float( ('.'.join(rfname.split('/')[-1].split(".")[:-1]).split("_")[1]) )
    # print float(mH)
    rf = R.TFile(rfname)
    if alg_type == 'CLS':
        hist = rf.Get("limit")
    elif alg_type == "Hypo":
        hist = rf.Get("result_mu")
    elif alg_type == 'CLSplusB':
        hist = rf.Get("limit_CLSsb")
    limit = LimitAtMh(mH, hist, alg_type)
    # if limit.status:
        # print "Fit status="+limit.status+" for mH="+str(mH)+" - skiiping "
        # continue
    results[mH] = limit

def frange(a, b, c):
    while a < b:
        yield a;
        a += c

run1_dic = {}
for mH in frange(15.0, 60.1, 0.5):
    if mH <= 20.0:
        run1_dic[mH] = 0.185
    elif mH <= 21.0:
        run1_dic[mH] = 0.265
    elif mH <= 23.0:
        run1_dic[mH] = 0.185
    elif mH <= 24.0:
        run1_dic[mH] = 0.255
    elif mH <= 24.5:
        run1_dic[mH] = 0.258
    elif mH <= 25.0:
        run1_dic[mH] = 0.260
    elif mH <= 25.5:
        run1_dic[mH] = 0.262
    elif mH <= 26.0:
        run1_dic[mH] = 0.257
    elif mH <= 26.5:
        run1_dic[mH] = 0.255
    elif mH <= 30.0:
        run1_dic[mH] = 0.185
    elif mH <= 30.0:
        run1_dic[mH] = 0.185
    elif mH <= 35.0:
        run1_dic[mH] = 0.183
    elif mH <= 37.5:
        run1_dic[mH] = 0.180
    elif mH <= 45.0:
        run1_dic[mH] = 0.178
    elif mH <= 55.0:
        run1_dic[mH] = 0.177
    elif mH <= 60.0:
        run1_dic[mH] = 0.178


masses = sorted(results.keys())
obss = [ results[mH].obs for mH in masses ] 
medians = [ results[mH].med for mH in masses ] 
plus2sigmas = [ results[mH].plus2sigma for mH in masses ] 
plus1sigmas = [ results[mH].plus1sigma for mH in masses ] 
minus2sigmas = [ results[mH].minus2sigma for mH in masses ] 
minus1sigmas = [ results[mH].minus1sigma for mH in masses ] 
# lim_run1r = [ run1_dic[mH] for mH in masses]
psplusb = [ results[mH].psplusb for mH in masses ] 


# fn_br = 'BR_ratio.txt'#'BR_emu.txt'



#for i,mH in enumerate(masses):
    #print mH, br[i], xs[i], cx_VBF[i], cx_ZH[i], cx_WH[i]

#cx_br

lim_run1 =[]
lim_obs =[]
lim_med =[]
minus1sig=[]
plus1sig=[]
minus2sig=[]
plus2sig=[]
# psplusb=[]

minus1sig_err = []
minus2sig_err = []
plus1sig_err = []
plus2sig_err = []

sm=[]

# we are still BLINDED
# this is requested by Ketevi, for his plotting
# obss = medians


def scan_hist(hist, masses):
    file_masses = []
    vals = []
    for i in xrange(hist.GetNbinsX()):
        file_masses.append(hist.GetBinCenter(i))
        vals.append(hist.GetBinContent(i))
    cx = [ linear_interp(mH, file_masses, vals) for mH in masses ] 
    return cx

# read efficiency and invPurity from the file
# factors = []
# if channel in ["4e","4m","2e2m"]:
    # sig_yield = get_cxs("PostInput/"+channel[1:]+'_yield.tsv', masses)
    # chan = channel[1:]
    # if "m" in channel:
        # chan += "u"
    # eff_file = R.TFile(eff_file_name, "READ")
    # invPur_hist = eff_file.Get("Nominal/eff_"+chan)
    # # invPur_hist.SetDirectory(0)
    # invPur = scan_hist(invPur_hist, masses)
    # eff_hist = eff_file.Get("Nominal/invPurity_"+chan)
    # # eff_hist.SetDirectory(0)
    # effs = scan_hist(eff_hist, masses)
    # print sig_yield, invPur, effs
    # for i in xrange(len(sig_yield)):
        # factors.append(1.0 * sig_yield[i] / (Lumi * effs[i] * invPur[i]))
    # print factors

outfile = open(tag+"/"+savename+".tsv",'w')
file_fid_xsecs = open(tag+"/"+channel+"_fid_xsecs_obs.tsv",'w')
outfile.write('\t'.join(
    ["#","mass","Obs","median","-1sig","+1sig","-2sig","+2sig"])+'\n')
file_fid_xsecs.write('\t'.join(
    ["#","mass","Obs","median","-1sig","+1sig","-2sig","+2sig"])+'\n')
for i in range(0,len(medians)):
    #lim_med.append(medians[i])
    #print float(br[i])*float(xs[i])*medians[i]*1000
    #print medians[i]*1000
    #print medians[i]
    #print float(br[i])*float(xs[i])*minus1sigmas[i]*1000
    #print float(br[i])*float(xs[i])*minus2sigmas[i]*1000
    #print float(br[i])*float(xs[i])*plus1sigmas[i]*1000
    #print float(br[i])*float(xs[i])*plus2sigmas[i]*1000

    #lim_med.append(medians[i])
    #minus1sig.append(minus1sigmas[i])
    #minus2sig.append(minus2sigmas[i])
    #plus1sig.append(plus1sigmas[i])
    #plus2sig.append(plus2sigmas[i])

    #lim_med.append(float(br[i])*float(xs[i])*medians[i]*1000)
    #minus1sig.append(float(br[i])*float(xs[i])*minus1sigmas[i]*1000)
    #minus2sig.append(float(br[i])*float(xs[i])*minus2sigmas[i]*1000)
    #plus1sig.append(float(br[i])*float(xs[i])*plus1sigmas[i]*1000)
    #plus2sig.append(float(br[i])*float(xs[i])*plus2sigmas[i]*1000)
    #sm.append(float(br[i])*float(xs[i])*1000)
    out_string = '\t'.join([str(masses[i]),str(obss[i]),str(medians[i]), str(minus1sigmas[i]), str(plus1sigmas[i]), str(minus2sigmas[i]), str(plus2sigmas[i])]) + '\n'
    # if channel in ["4e","4m","2e2m"]:
        # out_string_xsec = '\t'.join([str(masses[i]),str(obss[i]*factors[i]),str(medians[i]*factors[i]), str(minus1sigmas[i]*factors[i]), str(plus1sigmas[i]*factors[i]), str(minus2sigmas[i]*factors[i]), str(plus2sigmas[i]*factors[i])]) + '\n'
        # file_fid_xsecs.write(out_string_xsec)
    print "%4.2f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f \& %0.4f \\ " % (float(masses[i]), float(obss[i]), float(minus2sigmas[i]), float(minus1sigmas[i]), float(medians[i]), float(plus1sigmas[i]), float(plus2sigmas[i]), float(psplusb[i]))
      #print float(minus2sigmas[i]), float(minus1sigmas[i]), float(medians[i]), float(plus1sigmas[i]), float(plus2sigmas[i])
      #print "%0.2f" % (float(medians[i]))
    outfile.write(out_string)
    # lim_run1.append(lim_run1r[i])
    # list_psplusb.append(psplusb[i])
    lim_obs.append(obss[i])
    lim_med.append(medians[i])
    minus1sig.append(minus1sigmas[i])
    minus1sig_err.append(medians[i] - minus1sigmas[i])
    minus2sig.append(minus2sigmas[i])
    minus2sig_err.append(medians[i] - minus2sigmas[i])
    plus1sig.append(plus1sigmas[i])
    plus1sig_err.append(plus1sigmas[i] - medians[i] )
    plus2sig.append(plus2sigmas[i])
    plus2sig_err.append(plus2sigmas[i] - medians[i] )
    sm.append(1)

    #print lim_med[i]

#gr_med = PyROOTUtils.Graph(masses, medians)

gr_med = PyROOTUtils.Graph(masses, lim_med)
#gr_med.SetMarkerStyle(7)
gr_med.SetLineStyle(R.kDashed)
gr_med.SetLineWidth(2)
gr_med.SetMarkerSize(0.8)

gr_obs = PyROOTUtils.Graph(masses, lim_obs)
#gr_obs.SetLineStyle(R.kDashed)
gr_obs.SetLineWidth(2)
gr_obs.SetMarkerSize(0.4)

#band_1sigma = PyROOTUtils.Band(masses, minus1sigmas, plus1sigmas, fillColor=R.kGreen)
#band_2sigma = PyROOTUtils.Band(masses, minus2sigmas, plus2sigmas, fillColor=R.kYellow)
band_1sigma = PyROOTUtils.Band(masses, minus1sig, plus1sig, fillColor=R.kGreen)
band_2sigma = PyROOTUtils.Band(masses, minus2sig, plus2sig, fillColor=R.kYellow)

# Prepare frame
hframe = R.TH1I("frame", "frame", 1, min(masses), max(masses))
hframe.GetXaxis().SetTitle("Truth m_{HH} [GeV]")
hframe.GetXaxis().SetLabelOffset(0.01);
hframe.GetXaxis().SetTitleSize(0.04);
hframe.GetXaxis().SetLabelSize(0.04);
hframe.GetYaxis().SetTitle("95% Limit on m_{x}->HH cross section (pb)")
#hframe.GetYaxis().SetTitle(yname)
hframe.GetYaxis().SetLabelSize(0.04);
hframe.GetYaxis().SetTitleSize(0.04);
hframe.GetYaxis().SetTitleOffset(1.7);
c = R.TCanvas("c", "c", 600, 600)
# c.SetLeftMargin(0.15)
# c.SetLogx()
c.SetLogy()
hframe.Draw()
#1fb
#hframe.SetMaximum(200)
#hframe.SetMinimum(1)
#3fb
ch_val = 1.7
MaximumY = {"4e":ch_val,
        "2e2m":ch_val,
        "4m":ch_val,
        # "c4l":10,
        "c4l":0.50,
        "combined":0.50
        }
#hframe.SetMaximum(float(ymax))
hframe.SetMaximum(100.0)
hframe.SetMinimum(0.01)
band_2sigma.Draw("f")
band_1sigma.Draw("f")
hframe.Draw("axis,same")

# Draw exclusion line
line = R.TLine(min(masses), 1, max(masses), 1)
line.SetLineStyle(R.kDotted)
line.SetLineColor(R.kRed)
#line.Draw()

# Draw run1 result
# run1line = PyROOTUtils.Graph(masses, lim_run1)
# # run1line.SetLineStyle(R.kDotted)
# run1line.SetLineWidth(2)
# run1line.SetMarkerSize(0.8)
# run1line.SetLineColor(R.kRed)
# if channel == 'c4l' or channel == 'combined':
    # run1line.Draw("L")

a1 = R.TF1("fa1","[0]/x",0.01,10);
a1.SetLineColor(R.kRed)
a1.SetParName(0,"c1")
# gr_med.Fit("fa1")
gr_med.Draw("L")
#gr_obs.Draw("PL")

sm_pred = PyROOTUtils.Graph(masses, sm)
sm_pred.SetLineWidth(2)
sm_pred.SetLineColor(R.kRed)
#sm_pred.Draw("L,same")

# Draw Legend
leg = PyROOTUtils.Legend(0.62, 0.8, textSize=0.037)
# if channel == 'c4l' or channel == 'combined':
    # leg.AddEntry(run1line, "Run1 Observed", "l")
leg.AddEntry(gr_med, "Expected", "l")
#leg.AddEntry(gr_obs, "Observed", "l")
leg.AddEntry(band_1sigma, "#pm1#sigma", "f")
leg.AddEntry(band_2sigma, "#pm2#sigma", "f")
#leg.AddEntry(sm_pred, "#sigma_{SM} x BR", "l")
leg.Draw()

# Draw Text
col_l = 0.19
#AtlasUtil.AtlasLabelPreliminary(col_l, 0.85)
#AtlasUtil.AtlasLabel(col_l, 0.85)
AtlasUtil.AtlasLabelInternal(col_l, 0.85)
#AtlasUtil.DrawLuminosityFbEcmFirst(col_l, 0.78, 20.3, size=0.037, sqrts=8)
#AtlasUtil.DrawLuminosityFb(col_l, 0.78, 2.0, 13)
AtlasUtil.DrawLuminosityFb(col_l, 0.78, Lumi, 13)
# AtlasUtil.DrawText(col_l, 0.71, (channel[1:] if channel != "combined" else "Combined") + " Channel", size=0.037)#mu
# AtlasUtil.DrawText(col_l, 0.71, (channel if channel != "4l" else "Combined") + " Channel", size=0.037)#mu
# AtlasUtil.DrawText(col_l, 0.71, "H #rightarrow ZdZd #rightarrow 4#font[12]{" + 
        # channel + "}", size=0.037)#mu

# c.SaveAs("./limitplots/"+channel+"i_mu_obs.png")
# c.SaveAs("./limitplots/"+channel+"_mu_obs.eps")
c.SaveAs(tag+"/"+savename+".pdf")

c2 = R.TCanvas("c2","c2",600,600)
hframe.GetXaxis().SetTitle("m_{Z_{d}} [GeV]")
hframe.GetXaxis().SetLabelOffset(0.01);
hframe.GetXaxis().SetTitleSize(0.04);
hframe.GetXaxis().SetLabelSize(0.04);
hframe.GetYaxis().SetTitle("P_{S+B} at mu=0.01")
hframe.GetYaxis().SetLabelSize(0.04);
hframe.GetYaxis().SetTitleSize(0.04);
hframe.GetYaxis().SetTitleOffset(1.7);
hframe.SetMaximum(2)
hframe.SetMinimum(0.001)
hframe.Draw("axis,same")

gr_psplusb = PyROOTUtils.Graph(masses, psplusb)
gr_psplusb.SetLineWidth(2)
gr_psplusb.SetMarkerSize(0.8)
gr_psplusb.SetLineColor(R.kRed)
gr_psplusb.Draw("L")

c2.SaveAs(tag+"/"+channel+"_Psplusb.pdf")


ofile = R.TFile(tag + "/"+savename+".root", "RECREATE")
g_obs = R.TGraphErrors(
        len(masses),
        array('d',[a for a in masses]),
        array('d',[a for a in lim_obs]),
        array('d',[0 for a in masses]),
        array('d',[0 for a in lim_obs])
        )
g_obs.SetName("CLs_observed")
g_obs.Write()
g_med = R.TGraphErrors(
        len(masses),
        array('d',[a for a in masses]),
        array('d',[a for a in lim_med]),
        array('d',[0 for a in masses]),
        array('d',[0 for a in lim_med])
        )
g_med.SetName("Expected")
g_med.Write()
g_1sig = R.TGraphAsymmErrors(
        len(masses),
        array('d',[a for a in masses]),
        array('d',[a for a in lim_med]),
        array('d',[0 for a in masses]),
        array('d',[0 for a in masses]),
        array('d',[a for a in minus1sig_err]),
        array('d',[a for a in plus1sig_err])
        )
g_1sig.SetName("sig1")
g_1sig.Write()
g_2sig = R.TGraphAsymmErrors(
        len(masses),
        array('d',[a for a in masses]),
        array('d',[a for a in lim_med]),
        array('d',[0 for a in masses]),
        array('d',[0 for a in masses]),
        array('d',[a for a in minus2sig_err]),
        array('d',[a for a in plus2sig_err])
        )
g_2sig.SetName("sig2")
g_2sig.Write()
# band_1sigma.SetName("1sigma")
# band_1sigma.Write()
# band_2sigma.SetName("2sigma")
# band_2sigma.Write()
# sm_pred.SetName("sm")
# sm_pred.Write()
ofile.Close()
