import sys
sys.path.insert (0,'..')
import bbll

from ROOT import TMVA, TFile, TTree, TCut, TString
#DNN Library Pytorch
import torch
from torch.utils.data import DataLoader, TensorDataset
from torch import Tensor
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
# Optimizer SWATS 
import swats
#numpy, pandas, matplotlib
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
from importlib import reload
import pandas as pd
import random
from array import array
#ML library
import pickle
import scikitplot as skplt
import seaborn as sns
import sklearn
from sklearn import preprocessing
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.metrics import classification_report, accuracy_score
from sklearn.utils import shuffle
#XGBoost Library
import xgboost as xgb
from xgboost import XGBClassifier
from xgboost import plot_tree
#from IPython import display
#import graphviz
#Cuda Library
from numba import cuda
import pycuda.autoinit
import pycuda.driver as drv
from pycuda.compiler import SourceModule
from math import sqrt
import gc 
from scipy.stats import poisson
import multiprocessing
import time
import cuda_guass_normal

#Load tree for input feature normalization
signalFile = TFile.Open("../../sample-train-0313/tree_sig_reweighted.root")
backgroundFile = TFile.Open("../../sample-train-0313/tree_bkg_reweighted.root")
signalTree = signalFile.Get("ntup")
backgroundTree = backgroundFile.Get("ntup")
signalNum = signalTree.GetEntries()
backgroundNum = backgroundTree.GetEntries()
print("signal events number before cut is: ", signalNum)
print("background events number before cut is: ", backgroundNum)
# Set region

region=4

weight_name="reweighted_weight"

if((region == 2) | (region ==4)):
	variable_names=[weight_name,"process_id","m_hh_truth","is_em","is_me","bjet_0_pt","bjet_0_eta","bjet_1_pt","bjet_1_eta","ll_m","ll_pt","ll_deltar","ll_deltaeta",
			"ll_deltaphi","bb_m","bb_pt","bb_deltar","bb_deltaeta","bb_deltapt","met_met","met_sig","met_phi","mt_lep0_met","mt_lep1_met","m_bbll","m_bbllmet","ht2","ht2r"]
if((region == 1) | (region ==3)):
	variable_names=[weight_name,"process_id","m_hh_truth","is_ee","is_mm","bjet_0_pt","bjet_0_eta","bjet_1_pt","bjet_1_eta","ll_m","ll_pt","ll_deltar","ll_deltaeta",
			 "ll_deltaphi","bb_m","bb_pt","bb_deltar","bb_deltaeta","bb_deltapt","met_met","met_sig","met_phi","mt_lep0_met","mt_lep1_met","m_bbll","m_bbllmet","ht2","ht2r"]

if((region == 1) | (region == 3)):
	unused_variable=[weight_name,"is_ee","is_mm","process_id","m_hh_truth"]
if((region == 2) | (region == 4)):
	unused_variable=[weight_name,"is_em","is_me","process_id","m_hh_truth"]
dim = len(variable_names)-len(unused_variable)

signalArray = signalTree.AsMatrix(variable_names)
backgroundArray = backgroundTree.AsMatrix(variable_names)
signal_df = pd.DataFrame(data=signalArray, columns=variable_names)
background_df = pd.DataFrame(data=backgroundArray, columns=variable_names)
#select non-resonant
signal_df=signal_df[(signal_df["m_hh_truth"]==0)]
background_df=background_df[(background_df["m_hh_truth"]==0)]
background_df=background_df[(background_df["process_id"]!=100)]
#################################################################
if(region == 1):
    signal_df_region = signal_df[(signal_df["ll_m"] > 75.0) & (
        (signal_df["is_ee"] == 1) | (signal_df["is_mm"] == 1))]
    background_df_region = background_df[(background_df["ll_m"] > 75.0) & (
        (background_df["is_ee"] == 1) | (background_df["is_mm"] == 1))]
if(region == 2):
    signal_df_region = signal_df[(signal_df["ll_m"] > 75.0) & (
        (signal_df["is_em"] == 1) | (signal_df["is_me"] == 1))]
    background_df_region = background_df[(background_df["ll_m"] > 75.0) & (
        (background_df["is_em"] == 1) | (background_df["is_me"] == 1))]

if(region == 3):
    signal_df_region = signal_df[(signal_df["ll_m"] < 75.0) & (
        (signal_df["is_ee"] == 1) | (signal_df["is_mm"] == 1))]
    background_df_region = background_df[(background_df["ll_m"] < 75.0) & (
        (background_df["is_ee"] == 1) | (background_df["is_mm"] == 1))]
if(region == 4):
    signal_df_region = signal_df[(signal_df["ll_m"] < 75.0) & (
        (signal_df["is_em"] == 1) | (signal_df["is_me"] == 1))]
    background_df_region = background_df[(background_df["ll_m"] < 75.0) & (
        (background_df["is_em"] == 1) | (background_df["is_me"] == 1))]

signal_df_cut = signal_df_region[(signal_df_region["bb_m"] > 80.0) & (
    signal_df_region["bb_m"] < 150.0) & (signal_df_region["bb_deltar"] < 2.8)]
background_df_cut = background_df_region[(background_df_region["bb_m"] > 80.0) & (
    background_df_region["bb_m"] < 150.0) & (background_df_region["bb_deltar"] < 2.8)]

SigYield1 = signal_df_cut[weight_name].sum()
BkgYield1 = background_df_cut[weight_name].sum()
SBratio = SigYield1/BkgYield1
print("signal yield in region", region, " after precut is: ", SigYield1)
print("background yield in region", region, " after precut is: ", BkgYield1)
print("S/B ratio is: ", SBratio)
dataNormal_pre = signal_df_cut.append(background_df_cut)
dataNormal_weight_df = dataNormal_pre[weight_name]
dataNormal_df=dataNormal_pre.drop(columns=unused_variable)
dataNormal = dataNormal_df.values
dataNormal_weight = dataNormal_weight_df.values
reload(cuda_guass_normal)
mean_vec, var_vec = cuda_guass_normal.cuda_mean_var(dataNormal,dataNormal_weight)
del dataNormal
del dataNormal_df
del signalArray
del backgroundArray

sample_list=["bkg_top.root","bkg_DY.root","bkg_fakes.root","bkg_diboson.root","bkg_Higgs.root","tree_sig.root","data.root"]
#sample_list=["sig_bbZZ.root","sig_bbWW.root","sig_bbtautau.root","bkg_ttbar.root",
#            "bkg_DY.root","bkg_Higgs.root","bkg_fake.root","bkg_diboson.root","data_HH.root"]
#sample_list=["tree_bkg_2tag.root","tree_sig_2tag.root"]
#sample_list=["tree_sig.root","tree_bkg.root","tree_data.root"]
feature_names=["bjet_0_pt","bjet_0_eta","bjet_1_pt","bjet_1_eta","ll_m","ll_pt","ll_deltar","ll_deltaeta","ll_deltaphi","bb_m","bb_pt","bb_deltar","bb_deltaeta","bb_deltapt","met_met","met_sig","met_phi","mt_lep0_met","mt_lep1_met","m_bbll","m_bbllmet","ht2","ht2r"]
model_suffix='../NN_model_2tag/region4-0313-'
model_list=[model_suffix+'section0.pt',model_suffix+'section1.pt',model_suffix+'section2.pt',model_suffix+'section3.pt',model_suffix+'section4.pt']
isRes=1
for sample_name in sample_list:
    print("Applying ",sample_name)
    sample="/lustre/samples/di-higgs/sample-applied-0313/"+sample_name
    sigScore=bbll.apply_dnn(model_list, isRes, sample, "ntup", feature_names ,"pytorch_Region4", mean_vec, var_vec)

    
    
    
    
