import sys
sys.path.insert (0,'..')
import bbll

from ROOT import TMVA, TFile, TTree, TCut, TString
import multiprocessing
import numpy as np
import pandas as pd
import random
from array import array
from importlib import reload
from sklearn.utils import shuffle
import cuda_guass_normal
#load files
signalFile = TFile.Open("../../sample-train-0320/tree_sig.root")
backgroundFile = TFile.Open("../../sample-train-0320/tree_bkg_reweighted.root")
#signalFile = TFile.Open("/data1/shuzhou/samples/dihiggs/200127_V03/dl1r_70/2tag_sr1_sig.root")
#backgroundFile = TFile.Open("/data1/shuzhou/samples/dihiggs/200127_V03/dl1r_70/2tag_sr1_bkg.root")
signalTree = signalFile.Get("ntup")
backgroundTree = backgroundFile.Get("ntup")

signalNum = signalTree.GetEntries()
backgroundNum = backgroundTree.GetEntries()
print("signal events number before cut is: ", signalNum)
print("background events number before cut is: ", backgroundNum)
#weight_name="reweighted_weight"
weight_name="reweighted_weight"

num_fold=5

variable_names=[weight_name,"process_id","m_hh_truth","is_em","is_me","bjet_0_pt","bjet_0_eta","bjet_1_pt","bjet_1_eta","ll_m","ll_pt","ll_deltar","ll_deltaeta","ll_deltaphi","bb_m",
                              "bb_pt","bb_deltar","bb_deltaeta","bb_deltapt","met_met","met_sig","met_phi","mt_lep0_met","mt_lep1_met","m_bbll","m_bbllmet","ht2","ht2r"]

print("Loading signal and background trees, please be patient")
unused_variable=[weight_name,"is_em","is_me","process_id","m_hh_truth"]
dim = len(variable_names)-len(unused_variable)
# convert minitree to numpy arrays
signalArray=signalTree.AsMatrix(variable_names)

backgroundArray=backgroundTree.AsMatrix(variable_names)
    
print(signalArray.shape)
print("signal yield before cut is: ",sum(signalArray[:,0]))
print("background yield before cut is:",sum(backgroundArray[:,0]))

signal_df=pd.DataFrame(data=signalArray,columns=variable_names)
background_df=pd.DataFrame(data=backgroundArray,columns=variable_names)
#region 4 selection
#select non-resonant
signal_df=signal_df[(signal_df["m_hh_truth"]==0)]
background_df=background_df[(background_df["m_hh_truth"]==0)]
background_df=background_df[(background_df["process_id"]!=100)]
#################################################################
signal_df_region4=signal_df[(signal_df["ll_m"]<75.0)&((signal_df["is_em"]==1)|(signal_df["is_me"]==1))]
background_df_region4=background_df[(background_df["ll_m"]<75.0)&((background_df["is_em"]==1)|(background_df["is_me"]==1))]
SigYield=signal_df_region4[weight_name].sum()
BkgYield=background_df_region4[weight_name].sum()
SBratio=SigYield/BkgYield
print("signal yield in region 4 is: ",SigYield)
print("background yield in region 4 is: ",BkgYield)
print("S/B ratio is: ",SBratio)
# pre-train cuts
signal_df_cut=signal_df_region4[(signal_df_region4["bb_m"]>80.0)&(signal_df_region4["bb_m"]<150.0)&(signal_df_region4["bb_deltar"]<2.8)]
background_df_cut=background_df_region4[(background_df_region4["bb_m"]>80.0)&(background_df_region4["bb_m"]<150.0)&(background_df_region4["bb_deltar"]<2.8)]
#signal_df_cut=signal_df_region4[(signal_df_region4["bb_m"]>100000.0)&(signal_df_region4["bb_m"]<140000.0)]
#background_df_cut=background_df_region4[(background_df_region4["bb_m"]>100000.0)&(background_df_region4["bb_m"]<140000.0)]

SigYield1=signal_df_cut[weight_name].sum()
BkgYield1=background_df_cut[weight_name].sum()
SBratio=SigYield1/BkgYield1
print("signal yield in region 4 after precut is: ",SigYield1)
print("background yield in region 4 after precut is: ",BkgYield1)
print("S/B ratio is: ",SBratio)
#prepare numpys used for normaliztion
dataNormal_pre=signal_df_cut.append(background_df_cut)
dataNormal_weight_df=dataNormal_pre[weight_name]
dataNormal_df=dataNormal_pre.drop(columns=unused_variable)
dataNormal_df.describe()

dataNormal=dataNormal_df.values
dataNormal_weight=dataNormal_weight_df.values
reload(cuda_guass_normal)
mean_vec, var_vec = cuda_guass_normal.cuda_mean_var(dataNormal,dataNormal_weight)
# Add label to signal and background
signal_df_cut["label"]=1
background_df_cut["label"]=0

# Drop negative weighted events to import stability 
signal_df_pos = signal_df_cut[signal_df_cut[weight_name]>0]
background_df_pos = background_df_cut[background_df_cut[weight_name]>0]

signal_df_pos=shuffle(signal_df_pos)
background_df_pos = shuffle(background_df_pos)

signal_df_pos=shuffle(signal_df_pos)
background_df_pos = shuffle(background_df_pos)
# count signal and background size


del dataNormal
del dataNormal_df
del signalArray
del backgroundArray

#n_fold training
batch_size = 5000
max_epoch=300
patient=4
drop_variable =[weight_name, 'label', 'is_em', 'is_me','process_id','m_hh_truth']
model_name="../NN_model_2tag/region4-0313-section"
net_parameter = {}
optim_para = {}
optim_para["type"] = "sgd"
optim_para["lr"] = 1e-3
optim_para["beta"] = 0.9
net_parameter["num_cpu"] = multiprocessing.cpu_count()-2
net_parameter["optimizer"] = optim_para
net_parameter["input_size"] = dim
net_parameter["device"] = "gpu"
net_parameter["model_type"]=3
net_parameter["dim_list"] = [dim, 100,200,300,200,300]
net_parameter["drop_out"] = [False, True, True,True,True,True]
net_parameter["drop_out_rate"] = [0.2,0.2,0.2,0.2,0.2,0.2]
res_param = {}
res_param["input_dim"] =dim
res_param["Res_dim"] = 100
res_param["num_res"] = 4
res_param["Res_depth"] = 5
res_param["dropout_rate"]=0.0
net_parameter["Res_parameters"] = res_param
bbll.train_n_fold(num_fold, dim, signal_df_pos, background_df_pos, signal_df_cut, background_df_cut,SigYield, BkgYield, mean_vec, var_vec, net_parameter, model_name, drop_variable, weight_name, batch_size, max_epoch, patient)
