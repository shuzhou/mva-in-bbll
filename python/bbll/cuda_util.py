import ctypes 
from ctypes import *
import numpy as np

#load cuda c library
libgpu = ctypes.cdll.LoadLibrary("/home/shuzhou/Documents/dihiggs/python/bbll/cuda/libgpu.so")

#load cuda functions
reduce_min = libgpu.reduce_min
reduce_max = libgpu.reduce_max
reduce_add = libgpu.reduce_add
reduce_var = libgpu.reduce_var
reduce_sum = libgpu.reduce_sum
histo = libgpu.histo
histo_weight = libgpu.histo_weight
count_duplicate = libgpu.count_duplicate
sort = libgpu.sort
matrix_multi = libgpu.matrix_multi

clip = libgpu.cuda_clip

#################################
reduce_sum.restype = c_double
reduce_sum.argtypes = [POINTER(c_float), c_int]
#################################
reduce_max.restype = c_float
reduce_max.argtypes = [POINTER(c_float), c_int]
#################################
reduce_min.restype = c_float
reduce_min.argtypes = [POINTER(c_float), c_int]
#################################
reduce_sum.restype = c_double
reduce_sum.argtypes = [POINTER(c_float), c_int]
#################################
reduce_add.restype = c_double
reduce_add.argtypes = [POINTER(c_float), POINTER(c_float), c_int]
#################################
reduce_var.restype = c_double
reduce_var.argtypes = [POINTER(c_float), POINTER(c_float), c_int, c_double]
#################################
sort.argtypes = [POINTER(c_float), POINTER(c_float), POINTER(c_int), c_int]
#################################
count_duplicate.restype = c_int
count_duplicate.argtypes = [POINTER(c_float), c_int]
#################################
histo.argtypes = [POINTER(c_float), POINTER(c_int), c_float, c_float, c_int, c_int]
#################################
histo_weight.argtypes = [POINTER(c_float), POINTER(c_float), POINTER(c_float), c_float, c_float, c_int, c_int]
#################################
matrix_multi.argtypes = [POINTER(c_float), POINTER(c_float), POINTER(c_float), c_int, c_int, c_int, c_int]
#################################
clip.argtypes = [POINTER(c_int), POINTER(c_int), c_int, c_int, c_int]
#################################


def cuda_add(input_vec, weight_vec = None):
    size = len(input_vec)
    if(weight_vec == None):
        input_np = np.float32(input_vec.copy(order = 'C'))
        input_pointer = cast(input_np.ctypes.data, POINTER(c_float))
        result = reduce_sum(input_pointer, size)
        
    else:
        input_np = np.float32(input_vec.copy(order = 'C'))
        input_pointer = cast(input_np.ctypes.data, POINTER(c_float))
        weight_np = np.float32(weight_vec.copy(order = 'C'))
        weight_pointer = cast(weight_np.ctypes.data, POINTER(c_float))
        result = reduce_sum(input_pointer, weight_pointer, size)
        
    return(result)
 
    
    
        
        
def cuda_min(input_vec):
    size = len(input_vec)
    input_np = np.float32(input_vec.copy(order = 'C'))
    input_pointer = cast(input_np.ctypes.data, POINTER(c_float))
    result = reduce_min(input_pointer, size)    
    return(result)


def cuda_max(input_vec):
    size = len(input_vec)
    input_np = np.float32(input_vec.copy(order = 'C'))
    input_pointer = cast(input_np.ctypes.data, POINTER(c_float))
    result = reduce_max(input_pointer, size)    
    return(result)

def cuda_sort(input_vec):
    ##merge sort on gpu
    size = len(input_vec)
    input_np = np.float32(input_vec.copy(order = 'C'))
    input_pointer = cast(input_np.ctypes.data, POINTER(c_float))
    output = np.zeros((size,1))
    output = output.reshape(-1)
    index = np.zeros((size,1))
    index = index.reshape(-1)
    output1=np.float32(output.copy(order='C'))
    index1 = np.int32(index.copy(order='C'))
    output_pointer = cast(output1.ctypes.data, POINTER(c_float))
    index_pointer = cast(index1.ctypes.data, POINTER(c_int))
    sort(input_pointer, output_pointer, index_pointer, size)
    output_np = np.array(output_pointer[:size])
    return(output_np)
    
def cuda_count_duplicate(input_vec):
    ##sort the input vec first
    size = len(input_vec)
    input_np = np.float32(input_vec.copy(order = 'C'))
    input_pointer = cast(input_np.ctypes.data, POINTER(c_float))
    output = np.zeros((size,1))
    output = output.reshape(-1)
    index = np.zeros((size,1))
    index = index.reshape(-1)
    output1=np.float32(output.copy(order='C'))
    index1 = np.int32(index.copy(order='C'))
    output_pointer = cast(output1.ctypes.data, POINTER(c_float))
    index_pointer = cast(index1.ctypes.data, POINTER(c_int))
    sort(input_pointer, output_pointer, index_pointer, size)
    ##the count duplicate number in the vector
    num_duplicate = count_duplicate(output_pointer, size)
    return(num_duplicate)
    


def cuda_histogram(input_vec, bins, weight_vec = None):
    size = len(input_vec)
    min_value = cuda_min(input_vec)
    max_value = cuda_max(input_vec)
    input_np = np.float32(input_vec.copy(order = 'C'))
    input_pointer = cast(input_np.ctypes.data, POINTER(c_float))
    histo_out = np.zeros((bins,1))
    histo_out = histo_out.reshape(-1)
    if(weight_vec.all() == None):
        histo_out = np.int32(histo_out.copy(order = 'C'))
        histo_out_pointer = cast(histo_out.ctypes.data, POINTER(c_int))
        histo(input_pointer, histo_out_pointer, min_value, max_value, size, bins)
        
    else:
        histo_out = np.float32(histo_out.copy(order = 'C'))
        histo_out_pointer = cast(histo_out.ctypes.data, POINTER(c_float))
        weight_np = np.float32(weight_vec.copy(order = 'C'))
        weight_pointer = cast(weight_np.ctypes.data, POINTER(c_float))
        histo_weight(input_pointer, weight_pointer, histo_out_pointer, min_value, max_value, size, bins)
        
        
    output_np = np.array(histo_out_pointer[:bins])
    return(output_np)
        

def cuda_matrix_multi(vec_a, vec_b):
    a_shape = vec_a.shape
    b_shape = vec_b.shape
    vec_a = vec_a.reshape(-1)
    vec_b = vec_b.reshape(-1)
    a_np = np.float32(vec_a.copy(order = 'C'))
    b_np = np.float32(vec_b.copy(order = 'C'))
    a_pointer = cast(a_np.ctypes.data, POINTER(c_float))
    b_pointer = cast(b_np.ctypes.data, POINTER(c_float))
    a_d = np.array(a_pointer[:a_shape[0]*a_shape[1]])
    b_d = np.array(b_pointer[:b_shape[0]*b_shape[1]])
    out_np = np.zeros((a_shape[0],b_shape[1]))
    out_np = out_np.reshape(-1)
    out_vec = np.float32(out_np.copy(order = 'C'))
    out_pointer = cast(out_vec.ctypes.data, POINTER(c_float))
    matrix_multi(a_pointer, b_pointer, out_pointer, a_shape[1], a_shape[0], b_shape[1], b_shape[0])
    output_np = np.array(out_pointer[:a_shape[0]*b_shape[1]])
    output_np = output_np.reshape(a_shape[0],b_shape[1])
    return(output_np)

def cuda_clip(input_vec, min_v, max_v):
    input_shape = input_vec.shape
    vec_input = input_vec.reshape(-1)
    output_vec = np.zeros(input_shape)
    output_vec = output_vec.reshape(-1)
    size = len(vec_input)
    input_po = np.int32(vec_input.copy(order = 'C'))
    output_po = np.int32(output_vec.copy(order = 'C'))
    input_pointer = cast(input_po.ctypes.data, POINTER(c_int))
    output_pointer = cast(output_po.ctypes.data, POINTER(c_int))
    clip(input_pointer, output_pointer, max_v, min_v, size)
    output_np = np.array(output_pointer[:size])
    output_np = output_np.reshape(input_shape)
    return(output_np)
    
    
    
    

