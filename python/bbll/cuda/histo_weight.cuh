#ifndef HISTO_WEIGHT       
#define HISTO_WEIGHT       

#include <stdio.h>

extern "C" 
void histo_weight(float *input_vec, float *weight_vec, float *output_vec, float min, float max, long long size, int bins);
#endif
