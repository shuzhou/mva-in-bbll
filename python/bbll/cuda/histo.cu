#include <stdio.h>             
#include <math.h>              
#include <cstdlib>
#include "histo.cuh"

__global__ void gpu_histo(float *input_vec, int *output_vec, float min, float max, float step, long long size, int bins){
	extern __shared__ int binContent[];
	int bin[100] = {0}; //maximun bins can handle;
	int tid = threadIdx.x;
	int index = blockIdx.x*blockDim.x+threadIdx.x;
	int stride = gridDim.x*blockDim.x;
	int binid=0;
	if(tid<bins){
		binContent[tid] = 0;
	}
	__syncthreads();
	for(int i=index;i<size;i=i+stride){
		binid = (int)((input_vec[i]-min)/step);
		bin[binid] = bin[binid]+1;
	}
	__syncthreads(); 
	for(int i=0;i<bins;i++){
		atomicAdd(&binContent[i],bin[i]);
	}
	__syncthreads(); 
	if(tid ==0){
		for(int i=0;i<bins;i++){
			output_vec[i+bins*blockIdx.x] = binContent[i];
		}
	}
}


void histo(float *input_vec, int *output_vec, float min, float max, long long size, int bins){
	const int num_threads = 1024;
	const int num_blocks = 68;
	int deviceId;
	int numberOfSMs;
	dim3 block_num(num_blocks,1,1);
	dim3 threads_per_block(num_threads,1,1);
	cudaGetDevice(&deviceId);
	cudaDeviceGetAttribute(&numberOfSMs, cudaDevAttrMultiProcessorCount, deviceId);
	float *a;
	int *binContent;
	float step;
	step=(max-min)/(float)bins;
	cudaMallocManaged(&a, size*sizeof(float));
	cudaMemcpy(a, input_vec, size*sizeof(float),cudaMemcpyHostToDevice);
	cudaMallocManaged(&binContent, bins*num_blocks*sizeof(int));
	cudaMemPrefetchAsync(binContent, sizeof(binContent), deviceId);
	gpu_histo<<<block_num,threads_per_block,bins*sizeof(int)>>> (a, binContent, min, max, step, size, bins);
	cudaDeviceSynchronize();
	cudaMemPrefetchAsync(binContent, sizeof(binContent), cudaCpuDeviceId);
	for(int i =0; i<bins;i++){
		output_vec[i]=0;
		for(int j=0;j<num_blocks;j++){
			output_vec[i] = output_vec[i]+binContent[i+bins*j];
		}
	}
}


