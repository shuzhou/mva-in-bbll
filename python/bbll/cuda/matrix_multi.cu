#include <stdio.h>
#include <math.h>
#include <cstdlib>
#include "matrix_multi.cuh"
__global__ void gpu_matrix_multi(float *a, float *b, float *c, int a_col, int a_row, int b_col, int b_row){
	__shared__ float sharedMemA[16][16];
	__shared__ float sharedMemB[16][16];
	int x=threadIdx.x;
	int y=threadIdx.y;
	int col=blockIdx.x*blockDim.x+x;
	int row=blockIdx.y*blockDim.y+y;
	int tile_size=16;
	float res=0;
	for(int i=0;i<(int)(ceil((float)a_col/(float)tile_size));i++){
		if(row<a_row&&(tile_size*i+x)<a_col){
			sharedMemA[y][x]=a[row*a_col+tile_size*i+x];
		}
		else{
			sharedMemA[y][x]=0;
		}
		if(col<b_col&&(tile_size*i+y)<b_row){
			sharedMemB[y][x]=b[(tile_size*i+y)*b_col+col];
		}
		else{
			sharedMemB[y][x]=0;
		}
		__syncthreads();
		for(int j=0;j<tile_size;j++){
			res=res+sharedMemA[y][j]*sharedMemB[j][x];
		}
		__syncthreads();
	}
	if(row<a_row&&col<b_col){
		c[row*b_col+col]=res;
	}
}

void matrix_multi(float *a, float *b, float *c, int a_col, int a_row, int b_col, int b_row){
	float *matrix_a;
	float *matrix_b;
	float *matrix_c;
	int tile_size=16;
	dim3 block_shape(tile_size,tile_size,1);
	dim3 grid_shape(ceil((float)b_col/(float)tile_size),ceil((float)a_row/(float)tile_size),1);
	int deviceId;
	cudaError_t addVectorsErr;
	cudaError_t asyncErr;
	cudaGetDevice(&deviceId);
	cudaMallocManaged(&matrix_a,a_col*a_row*sizeof(float));
	cudaMallocManaged(&matrix_b,b_col*b_row*sizeof(float));
	cudaMallocManaged(&matrix_c,b_col*a_row*sizeof(float));
	cudaMemcpy(matrix_a,a,a_col*a_row*sizeof(float),cudaMemcpyHostToDevice);
	cudaMemcpy(matrix_b,b,b_col*b_row*sizeof(float),cudaMemcpyHostToDevice);
	cudaMemPrefetchAsync(matrix_c,b_col*a_row*sizeof(float),deviceId);
	gpu_matrix_multi<<<grid_shape,block_shape>>>(matrix_a, matrix_b, matrix_c, a_col, a_row, b_col, b_row);
	addVectorsErr = cudaGetLastError();
	if(addVectorsErr != cudaSuccess) printf("Error: %s\n", cudaGetErrorString(addVectorsErr));
        asyncErr = cudaDeviceSynchronize();
        if(asyncErr != cudaSuccess) printf("Error: %s\n", cudaGetErrorString(asyncErr));
	cudaMemcpy(c,matrix_c, a_row*b_col*sizeof(float),cudaMemcpyDeviceToHost);
}
