#ifndef CUDA_CLIP       
#define CUDA_CLIP       

#include <stdio.h>

extern "C" 

void cuda_clip(int *input, int *output, int max, int min, int size);    
#endif
