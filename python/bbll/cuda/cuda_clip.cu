#include <stdio.h>
#include <math.h>
#include <cstdlib>
#include "cuda_clip.cuh"

__global__ void gpu_clip(int *input, int *output, int max, int min, int size){

	int index = blockIdx.x*blockDim.x+threadIdx.x;
	if(index<size){
		if(input[index]>max){
			output[index] = max;
		}
		else if(input[index]<min){
			output[index] = min;
		}
		else{
			output[index] = input[index];
		}
	}
}



void cuda_clip(int *input, int *output, int max, int min, int size){
	int *input_vec;
	int *output_vec;
	int num_threads = 1024;
	int num_blocks = ceil(((float)size)/((float)num_threads));
	dim3 block_shape(num_threads, 1, 1);
	dim3 grid_shape(num_blocks, 1, 1);
	int deviceId;
	cudaError_t addVectorsErr;
	cudaError_t asyncErr;
	cudaGetDevice(&deviceId);
	cudaMallocManaged(&input_vec, size*sizeof(int));
	cudaMallocManaged(&output_vec, size*sizeof(int));
	cudaMemcpy(input_vec, input, size*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemPrefetchAsync(output_vec, size*sizeof(int), deviceId);
	gpu_clip<<<grid_shape, block_shape>>> (input_vec, output_vec, max, min, size);
	addVectorsErr = cudaGetLastError();
	if(addVectorsErr != cudaSuccess) printf("Error: %s\n", cudaGetErrorString(addVectorsErr));
        asyncErr = cudaDeviceSynchronize();
        if(asyncErr != cudaSuccess) printf("Error: %s\n", cudaGetErrorString(asyncErr));
	cudaMemcpy(output, output_vec, size*sizeof(int), cudaMemcpyDeviceToHost);
}
