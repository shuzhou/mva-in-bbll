import sys
sys.path.insert (0,'..')
import bbll

from ROOT import TMVA, TFile, TTree, TCut, TString
#DNN Library Pytorch
import torch
from torch.utils.data import DataLoader, TensorDataset
from torch import Tensor
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
# Optimizer SWATS 
import swats
#numpy, pandas, matplotlib
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
from importlib import reload
import pandas as pd
import random
from array import array
#ML library
import pickle
import scikitplot as skplt
import seaborn as sns
import sklearn
from sklearn import preprocessing
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.metrics import classification_report, accuracy_score
from sklearn.utils import shuffle
#XGBoost Library
import xgboost as xgb
from xgboost import XGBClassifier
from xgboost import plot_tree
#from IPython import display
#import graphviz
#Cuda Library
from numba import cuda
import pycuda.autoinit
import pycuda.driver as drv
from pycuda.compiler import SourceModule
from math import sqrt
import gc 
from scipy.stats import poisson
import multiprocessing
import time

#Load tree for input feature normalization
signalFile = TFile.Open("../../sample-train-1118/tree_sig_region3.root")
backgroundFile = TFile.Open("../../sample-train-1118/tree_bkg_region3.root")
signalTree = signalFile.Get("ntupe")
backgroundTree = backgroundFile.Get("ntupe")
signalNum = signalTree.GetEntries()
backgroundNum = backgroundTree.GetEntries()
print("signal events number before cut is: ", signalNum)
print("background events number before cut is: ", backgroundNum)
# Set region

region=3


if((region == 2) | (region ==4)):
    variable_names=["weight","m_hh_truth","bjet_0_pt","bjet_0_eta","ll_m","ll_pt","ll_deltar",
                              "met_met","met_sig","met_phi","mt_lep0_met","mt_lep1_met","deltar_min_bl","deltar_max_bl","deltar_diff_bl","dphi_met_ll","dpt_met_ll","ht2","n_central_jets","n_forward_jets","m_bbllmet","otherjet_0_pt","otherjet_0_eta","otherjet_1_pt","otherjet_1_eta"]
if((region == 1) | (region ==3)):
    variable_names=["weight","m_hh_truth","bjet_0_pt","bjet_0_eta","ll_m","ll_pt","ll_deltar",
                              "met_met","met_sig","met_phi","mt_lep0_met","mt_lep1_met","deltar_min_bl","deltar_max_bl","deltar_diff_bl","dphi_met_ll","dpt_met_ll","ht2","n_central_jets","n_forward_jets","m_bbllmet","otherjet_0_pt","otherjet_0_eta","otherjet_1_pt","otherjet_1_eta"]
    

signalArray = signalTree.AsMatrix(variable_names)
backgroundArray = backgroundTree.AsMatrix(variable_names)
signal_df = pd.DataFrame(data=signalArray, columns=variable_names)
background_df = pd.DataFrame(data=backgroundArray, columns=variable_names)

signal_df=signal_df[(signal_df["m_hh_truth"]==0)]
background_df = background_df[(background_df["m_hh_truth"]==0)]
signal_df = signal_df.drop(columns=["m_hh_truth"])
background_df = background_df.drop(columns=["m_hh_truth"])



signal_df_region = signal_df 
background_df_region = background_df
signal_df_cut = signal_df_region#[(signal_df_region["bb_m"] > 80000.0) & (
    #signal_df_region["bb_m"] < 150000.0) & (signal_df_region["bb_deltar"] < 2.8)]
background_df_cut = background_df_region#[(background_df_region["bb_m"] > 80000.0) & (
    #background_df_region["bb_m"] < 150000.0) & (background_df_region["bb_deltar"] < 2.8)]
    
    
missing_value=-10000000000
print("Fill singal missing values")
sig_mean_vec = bbll.find_mean_value(signal_df_cut,"weight")
bkg_mean_vec = bbll.find_mean_value(background_df_cut,"weight")
data_mean_vec = bbll.find_mean_value(signal_df_cut.append(background_df_cut),"weight")

signal_df_cut = bbll.fill_missing_value_mean(signal_df_cut, "weight", missing_value,sig_mean_vec)
print("Fill background missing values")
background_df_cut = bbll.fill_missing_value_mean(background_df_cut, "weight", missing_value, bkg_mean_vec)

SigYield1 = signal_df_cut["weight"].sum()
BkgYield1 = background_df_cut["weight"].sum()
SBratio = SigYield1/BkgYield1
print("signal yield in region", region, " after precut is: ", SigYield1)
print("background yield in region", region, " after precut is: ", BkgYield1)
print("S/B ratio is: ", SBratio)
dataNormal_pre = signal_df_cut.append(background_df_cut)
dataNormal_weight_df = dataNormal_pre["weight"]
if((region == 4) | (region == 2)):
    dataNormal_df = dataNormal_pre.drop(columns=["weight"])
if((region == 3) | (region == 1)):
    dataNormal_df = dataNormal_pre.drop(columns=["weight"])
dataNormal_df.describe()
dataNormal = dataNormal_df.values
dataNormal_weight = dataNormal_weight_df.values


#sample_list=["sig_bbZZ.root","sig_bbWW.root","sig_bbtautau.root","data_HH.root","bkg_ttbar.root",
            #"bkg_DY.root","bkg_Higgs.root","bkg_fake.root","bkg_top.root","bkg_diboson.root"]
sample_list=["tree_sig_region3.root","tree_bkg_region3.root","tree_data_region3.root"]
sample_type = [1,0,2]
feature_names=["weight","bjet_0_pt","bjet_0_eta","ll_m","ll_pt","ll_deltar",
                              "met_met","met_sig","met_phi","mt_lep0_met","mt_lep1_met","deltar_min_bl","deltar_max_bl","deltar_diff_bl","dphi_met_ll","dpt_met_ll","ht2","n_central_jets","n_forward_jets","m_bbllmet","otherjet_0_pt","otherjet_0_eta","otherjet_1_pt","otherjet_1_eta"]

model_list=['../NN_model_1tag/region3-1118-section0.pt', '../NN_model_1tag/region3-1118-section1.pt', '../NN_model_1tag/region3-1118-section2.pt', '../NN_model_1tag/region3-1118-section3.pt', '../NN_model_1tag/region3-1118-section4.pt']
for (sample_name,s_type) in zip(sample_list, sample_type):
    print("Applying ",sample_name)
    sample="/lustre/samples/di-higgs/sample-applied-1119-1tag/"+sample_name
    if(s_type==1):
        sigScore=bbll.apply_dnn_fill_miss(model_list, sample, "ntupe", feature_names, "weight", "pytorch_region3", dataNormal, dataNormal_weight, sig_mean_vec, missing_value)
    if(s_type==0):
        sigScore=bbll.apply_dnn_fill_miss(model_list, sample, "ntupe", feature_names, "weight", "pytorch_region3", dataNormal, dataNormal_weight, bkg_mean_vec, missing_value)
    if(s_type==2):
        sigScore=bbll.apply_dnn_fill_miss(model_list, sample, "ntupe", feature_names, "weight", "pytorch_region3", dataNormal, dataNormal_weight, data_mean_vec, missing_value)
    
    
    
