import sys
sys.path.insert (0,'..')
import bbll

from ROOT import TMVA, TFile, TTree, TCut, TString
#DNN Library Pytorch
import torch
from torch.utils.data import DataLoader, TensorDataset
from torch import Tensor
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
# Optimizer SWATS 
import swats
#numpy, pandas, matplotlib
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
from importlib import reload
import pandas as pd
import random
from array import array
#ML library
import pickle
import scikitplot as skplt
import seaborn as sns
import sklearn
from sklearn import preprocessing
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.metrics import classification_report, accuracy_score
from sklearn.utils import shuffle
#XGBoost Library
import xgboost as xgb
from xgboost import XGBClassifier
from xgboost import plot_tree
#from IPython import display
#import graphviz
#Cuda Library
from numba import cuda
import pycuda.autoinit
import pycuda.driver as drv
from pycuda.compiler import SourceModule
from math import sqrt
import gc 
from scipy.stats import poisson
import multiprocessing
import time


import cuda_guass_normal
#load files
signalFile = TFile.Open("../../sample-train-1025/tree_sig_2tag.root")
backgroundFile = TFile.Open("../../sample-train-1025/tree_bkg_2tag.root")

signalTree = signalFile.Get("ntup")
backgroundTree = backgroundFile.Get("ntup")

signalNum = signalTree.GetEntries()
backgroundNum = backgroundTree.GetEntries()
print("signal events number before cut is: ", signalNum)
print("background events number before cut is: ", backgroundNum)

variable_names=["weight","is_em","is_me","bjet_0_pt","bjet_0_eta","bjet_1_pt","bjet_1_eta","ll_m","ll_pt","ll_deltar","ll_deltaeta","ll_deltaphi","bb_m",
                              "bb_pt","bb_deltar","bb_deltaeta","bb_deltapt","met_met","met_sig","met_phi","mt_lep0_met","mt_lep1_met","m_bbll","m_bbllmet"]

print("Loading signal and background trees, please be patient")
# convert minitree to numpy arrays
signalArray=signalTree.AsMatrix(variable_names)

backgroundArray=backgroundTree.AsMatrix(variable_names)
    
print(signalArray.shape)
print("signal yield before cut is: ",sum(signalArray[:,0]))
print("background yield before cut is:",sum(backgroundArray[:,0]))

signal_df=pd.DataFrame(data=signalArray,columns=variable_names)
background_df=pd.DataFrame(data=backgroundArray,columns=variable_names)
#region 4 selection
signal_df_region2=signal_df[(signal_df["ll_m"]>75000.0)&(signal_df["ll_m"]<110000.0)&((signal_df["is_em"]==1)|(signal_df["is_me"]==1))]
background_df_region2=background_df[(background_df["ll_m"]>75000.0)&(background_df["ll_m"]<110000.0)&((background_df["is_em"]==1)|(background_df["is_me"]==1))]
SigYield=signal_df_region2["weight"].sum()
BkgYield=background_df_region2["weight"].sum()
SBratio=SigYield/BkgYield
print("signal yield in region 2 is: ",SigYield)
print("background yield in region 2 is: ",BkgYield)
print("S/B ratio is: ",SBratio)
# pre-train cuts
signal_df_cut=signal_df_region2[(signal_df_region2["bb_m"]>80000.0)&(signal_df_region2["bb_m"]<150000.0)&(signal_df_region2["bb_deltar"]<2.8)]
background_df_cut=background_df_region2[(background_df_region2["bb_m"]>80000.0)&(background_df_region2["bb_m"]<150000.0)&(background_df_region2["bb_deltar"]<2.8)]
SigYield1=signal_df_cut["weight"].sum()
BkgYield1=background_df_cut["weight"].sum()
SBratio=SigYield1/BkgYield1
print("signal yield in region 2 after precut is: ",SigYield1)
print("background yield in region 2 after precut is: ",BkgYield1)
print("S/B ratio is: ",SBratio)
#prepare numpys used for normaliztion
dataNormal_pre=signal_df_cut.append(background_df_cut)
dataNormal_weight_df=dataNormal_pre["weight"]
dataNormal_df=dataNormal_pre.drop(columns=["weight","is_em","is_me"])
dataNormal_df.describe()

dataNormal=dataNormal_df.values
dataNormal_weight=dataNormal_weight_df.values
# Add label to signal and background
signal_df_cut["label"]=1
background_df_cut["label"]=0

# Drop negative weighted events to import stability 
signal_df_pos = signal_df_cut[signal_df_cut["weight"]>0]
background_df_pos = background_df_cut[background_df_cut["weight"]>0]

signal_df_pos=shuffle(signal_df_pos)
background_df_pos = shuffle(background_df_pos)

signal_df_pos=shuffle(signal_df_pos)
background_df_pos = shuffle(background_df_pos)
# count signal and background size

signal_size=signal_df_pos['weight'].count()
background_size=background_df_pos['weight'].count()
print(signal_size)
print(background_size)
# use 20% events as validation set, 5 folds folding 
signal_step = int(signal_size/5)
background_step = int(background_size/5)
print(signal_step)
print(background_step)
# scale up signal weights to equal total background yield 
signal_ratio=signal_df_pos["weight"].sum()/background_df_pos["weight"].sum()
print(signal_ratio)

signal_df_pos["weight"]=signal_df_pos["weight"]/signal_ratio
start_time = time.time()
# 5 fold training
for i in range(5):
    print("start section ",i)
    model_file = "../NN_model_2tag/region2-1025-section"+str(i)+".pt"
    signal_start = i*signal_step
    signal_end = (i+1)*signal_step
    background_start = i*background_step
    background_end = (i+1)*background_step
    print(signal_start, signal_end)
    print(background_start, background_end)
    #prepare training and testing set
    signal_df_test = signal_df_pos.iloc[signal_start:signal_end]
    signal_df_train = signal_df_pos.iloc[0:signal_start].append(signal_df_pos.iloc[signal_end:signal_size-1])
    background_df_test = background_df_pos.iloc[background_start:background_end]
    background_df_train = background_df_pos.iloc[0:background_start].append(background_df_pos.iloc[background_end:background_size-1])
    train_ratio=signal_df_train["weight"].sum()/background_df_train["weight"].sum()
    test_ratio=signal_df_test["weight"].sum()/background_df_test["weight"].sum()
    print(train_ratio)
    print(test_ratio)
    data_df_train=signal_df_train.append(background_df_train)
    data_df_test=signal_df_test.append(background_df_test)
    data_df_train=shuffle(data_df_train)
    data_df_test=shuffle(data_df_test)
    data_train_label=data_df_train["label"]
    data_train_weight=data_df_train["weight"]
    data_train_data=data_df_train.drop(columns=["weight","label","is_me","is_em"])
    data_test_label=data_df_test["label"]
    data_test_weight=data_df_test["weight"]
    data_test_data=data_df_test.drop(columns=["weight","label","is_me","is_em"])
    #prepare features, weight and label arrays
    dataTrainSample=data_train_data.values
    dataTestSample=data_test_data.values
    dataTrainWeight=data_train_weight.values
    dataTestWeight=data_test_weight.values
    dataTrainLabel=data_train_label.values
    dataTestLabel=data_test_label.values
    #prepare signal and background arrays for check
    signal_df_weight=signal_df_cut["weight"]
    signal_df_data=signal_df_cut.drop(columns=["weight","label","is_me","is_em"])
    background_df_weight=background_df_cut["weight"]
    background_df_data=background_df_cut.drop(columns=["weight","label","is_me","is_em"])
    signalSample=signal_df_data.values
    backgroundSample=background_df_data.values
    signalWeight=signal_df_weight.values
    backgroundWeight=background_df_weight.values
    #normalize input features
    print("Normalizing input features:")
    reload(cuda_guass_normal)

    dataTrainSample1=cuda_guass_normal.guass_normal(dataTrainSample,dataNormal,dataNormal_weight)
    dataTestSample1=cuda_guass_normal.guass_normal(dataTestSample,dataNormal,dataNormal_weight)

    signalSample1=cuda_guass_normal.guass_normal(signalSample,dataNormal,dataNormal_weight)
    backgroundSample1=cuda_guass_normal.guass_normal(backgroundSample,dataNormal,dataNormal_weight)
    num_cpu = multiprocessing.cpu_count()-7
    #create data loader
    print(num_cpu-1," cores will be used")
    drv.init()
    device = torch.device("cuda:0")
    print("Loading dataloaders")
    y_tensor = torch.tensor(dataTrainLabel, dtype=torch.float)  # .to(device)
    WeightTensor = torch.tensor((dataTrainWeight), dtype=torch.float)  # .to(device)
    dataTrainTensor = torch.tensor(dataTrainSample1, dtype=torch.float)  # .to(device)
    dataTestTensor = torch.tensor(dataTestSample1, dtype=torch.float)  # .to(device)
    TrainDataset = TensorDataset(WeightTensor, dataTrainTensor, y_tensor)
    TrainLoader = DataLoader(TrainDataset, batch_size=5000, shuffle=False, num_workers=num_cpu - 1, pin_memory=True)
    TestWeightTensor = torch.tensor(dataTestWeight, dtype=torch.float)  # .to(device)
    test_y_tensor = torch.tensor(dataTestLabel, dtype=torch.float)  # .to(device)
    TestDataset = TensorDataset((TestWeightTensor), dataTestTensor, test_y_tensor)
    TestLoader = DataLoader(TestDataset, batch_size=10000, shuffle=False, num_workers=num_cpu - 1, pin_memory=True)
    # load DNN model and init weights
    print("Creating models")
    model = bbll.Net().to(device)
    model.apply(bbll.init_weights)

    loss_fn = nn.BCELoss().cuda()
    # loss_fn=custom_loss()
    # optimizer = optim.Adam(model.parameters())
    optimizer = swats.SWATS(model.parameters())
    # optimizer = optim.SGD(model.parameters(), lr=1e-4, momentum=0.6)
    # scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min')
    #train the model
    print("Trainning model")
    loss_np, loss_test_np, best_epoch = bbll.train_model(model, device, TrainLoader, TestLoader, loss_fn, optimizer, 300, 4)
    del TrainLoader
    torch.cuda.empty_cache()
    del TrainDataset
    torch.cuda.empty_cache()
    # del dataTrainTensor
    # torch.cuda.empty_cache()
    del WeightTensor
    torch.cuda.empty_cache()
    print("Model saved to file",model_file)
    torch.save(model.state_dict(),model_file)
    print("Appling model to whole signal and background dataset")
    signalTensor = torch.tensor(signalSample1, dtype=torch.float, device=device)
    signalLoader=DataLoader(signalTensor,batch_size=10000, shuffle=False)

    sigScore = bbll.eval_pytorch(model,device,signalLoader)
    sigScore=sigScore[1:,0]

    bakTensor = torch.tensor(backgroundSample1, dtype=torch.float, device=device)
    backgroundLoader = DataLoader(bakTensor, batch_size=10000, shuffle=False)
    bakScore = bbll.eval_pytorch(model, device, backgroundLoader)

    bakScore = bakScore[1:, 0]
    del signalTensor
    del signalLoader
    del bakTensor
    del backgroundLoader
    torch.cuda.empty_cache()
    print("Calculate train AUC: ")
    dataTrainTensor= torch.tensor(dataTrainSample1, dtype=torch.float,device=device)
    dataTrainLoader=DataLoader(dataTrainTensor,batch_size=10000, shuffle=False)
    TrainScore = bbll.eval_pytorch(model,device,dataTrainLoader)
    TrainScore=TrainScore[1:,0]
    fpr, tpr, threshold=sklearn.metrics.roc_curve(dataTrainLabel,TrainScore,sample_weight=dataTrainWeight)
    roc_auc = sklearn.metrics.auc(fpr, tpr)
    print("Train AUC is: ", roc_auc)
    del dataTrainTensor
    del dataTrainLoader 
    torch.cuda.empty_cache()
    
    print("Calculate test AUC: ")
    dataTestTensor= torch.tensor(dataTestSample1, dtype=torch.float,device=device)
    dataTestLoader=DataLoader(dataTestTensor,batch_size=10000, shuffle=False)
    TestScore = bbll.eval_pytorch(model,device,dataTestLoader)
    TestScore=TestScore[1:,0]
    fpr, tpr, threshold=sklearn.metrics.roc_curve(dataTestLabel,TestScore,sample_weight=dataTestWeight)
    roc_auc = sklearn.metrics.auc(fpr, tpr)
    print("Test AUC is: ", roc_auc)
    del dataTestTensor
    del dataTestLoader 
    torch.cuda.empty_cache()
    
    reload(cuda_guass_normal)
    print("Some navie stats check: ")
    i = 0.1
    eff = (0.8, 0.7, 0.6, 0.5, 0.4, 0.3)
    j = 0
    k = 0
    t = 0
    min_diff = 100
    print(eff)
    for i in np.arange(0, 1, 0.001):
        if (j >= 6):
            break
        resS = cuda_guass_normal.cuda_cut(sigScore, signalWeight, i)
        ratio = resS / SigYield
        if (abs(ratio - eff[j]) < 0.1):
            t = 1
            if (k == 0):
                min_diff = 100
            if (abs(ratio - eff[j]) < min_diff):
                min_diff = abs(ratio - eff[j])
                BDT_cut = i
            k = k + 1

        if (abs(ratio - eff[j]) > 0.05):
            if (t == 1):
                j = j + 1
                k = 0
                t = 0
                resB = cuda_guass_normal.cuda_cut(bakScore, backgroundWeight, BDT_cut)
                resS = cuda_guass_normal.cuda_cut(sigScore, signalWeight, BDT_cut)
                SBratio = resS / resB
                Sig = resS / sqrt(resB)
                print("If cut NN at ", BDT_cut, "signal yield is:", resS, " Eff is:", resS / SigYield,
                      " background yield is:", resB,
                      " Eff is: ", resB / BkgYield, " S/B ratio is: ", SBratio, " Significance is: ", Sig)
      
    
print("Training finished, total time cost is: ", time.time()-start_time)
