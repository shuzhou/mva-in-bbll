import sys
sys.path.insert (0,'..')
import bbll

from ROOT import TMVA, TFile, TTree, TCut, TString
#DNN Library Pytorch
import torch
from torch.utils.data import DataLoader, TensorDataset
from torch import Tensor
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
# Optimizer SWATS 
import swats
#numpy, pandas, matplotlib
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
from importlib import reload
import pandas as pd
import random
from array import array
#ML library
import pickle
import scikitplot as skplt
import seaborn as sns
import sklearn
from sklearn import preprocessing
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.metrics import classification_report, accuracy_score
from sklearn.utils import shuffle
#XGBoost Library
import xgboost as xgb
from xgboost import XGBClassifier
from xgboost import plot_tree
#from IPython import display
#import graphviz
#Cuda Library
from numba import cuda
import pycuda.autoinit
import pycuda.driver as drv
from pycuda.compiler import SourceModule
from math import sqrt
import gc 
from scipy.stats import poisson
import multiprocessing
import time

import cuda_guass_normal

#Load tree for input feature normalization
signalFile = TFile.Open("../../sample-train-1118/tree_sig_region4.root")
backgroundFile = TFile.Open("../../sample-train-1118/tree_bkg_region4.root")
signalTree = signalFile.Get("ntupe")
backgroundTree = backgroundFile.Get("ntupe")
signalNum = signalTree.GetEntries()
backgroundNum = backgroundTree.GetEntries()
print("signal events number before cut is: ", signalNum)
print("background events number before cut is: ", backgroundNum)
# Set region

region=4


if((region == 2) | (region ==4)):
    variable_names=["weight","m_hh_truth","is_em","is_me","bjet_0_pt","bjet_0_eta","ll_m","ll_pt","ll_deltar",
                              "met_met","met_sig","mt_lep0_met","mt_lep1_met","deltar_min_bl","deltar_max_bl","deltar_diff_bl","dphi_met_ll","dpt_met_ll","ht2","ht2r","n_central_jets","n_forward_jets","m_bbllmet","otherjet_0_pt","otherjet_0_eta","met_phi_centrality","pt_bbll_scalar","pt_bbll_vector","pt_bbllmet_scalar","pt_bbllmet_vector"]
if((region == 1) | (region ==3)):
    variable_names=["weight","m_hh_truth","is_ee","is_mm","bjet_0_pt","bjet_0_eta","ll_m","ll_pt","ll_deltar",
                              "met_met","met_sig","mt_lep0_met","mt_lep1_met","deltar_min_bl","deltar_max_bl","deltar_diff_bl","dphi_met_ll","dpt_met_ll","ht2","ht2r","n_central_jets","n_forward_jets","m_bbllmet","otherjet_0_pt","otherjet_0_eta","met_phi_centrality","pt_bbll_scalar","pt_bbll_vector","pt_bbllmet_scalar","pt_bbllmet_vector"]
    

signalArray = signalTree.AsMatrix(variable_names)
backgroundArray = backgroundTree.AsMatrix(variable_names)
signal_df = pd.DataFrame(data=signalArray, columns=variable_names)
background_df = pd.DataFrame(data=backgroundArray, columns=variable_names)

signal_df=signal_df[(signal_df["m_hh_truth"]==0)]
background_df = background_df[(background_df["m_hh_truth"]==0)]
signal_df = signal_df.drop(columns=["m_hh_truth"])
background_df = background_df.drop(columns=["m_hh_truth"])



if(region == 1):
    signal_df_region = signal_df[(signal_df["ll_m"] > 75000.0) & (signal_df["ll_m"] < 110000.0) & (
        (signal_df["is_ee"] == 1) | (signal_df["is_mm"] == 1))]
    background_df_region = background_df[(background_df["ll_m"] > 75000.0) & (background_df["ll_m"] < 110000.0) & (
        (background_df["is_ee"] == 1) | (background_df["is_mm"] == 1))]
if(region == 2):
    signal_df_region = signal_df[(signal_df["ll_m"] > 75000.0) & (signal_df["ll_m"] < 110000.0) & (
        (signal_df["is_em"] == 1) | (signal_df["is_me"] == 1))]
    background_df_region = background_df[(background_df["ll_m"] > 75000.0) & (background_df["ll_m"] < 110000.0) & (
        (background_df["is_em"] == 1) | (background_df["is_me"] == 1))]

if(region == 3):
    signal_df_region = signal_df[(signal_df["ll_m"] < 75000.0) & (
        (signal_df["is_ee"] == 1) | (signal_df["is_mm"] == 1))]
    background_df_region = background_df[(background_df["ll_m"] < 75000.0) & (
        (background_df["is_ee"] == 1) | (background_df["is_mm"] == 1))]
if(region == 4):
    signal_df_region = signal_df[(signal_df["ll_m"] < 75000.0) & (
        (signal_df["is_em"] == 1) | (signal_df["is_me"] == 1))]
    background_df_region = background_df[(background_df["ll_m"] < 75000.0) & (
        (background_df["is_em"] == 1) | (background_df["is_me"] == 1))]
signal_df_cut = signal_df_region#[(signal_df_region["bb_m"] > 80000.0) & (
    #signal_df_region["bb_m"] < 150000.0) & (signal_df_region["bb_deltar"] < 2.8)]
background_df_cut = background_df_region#[(background_df_region["bb_m"] > 80000.0) & (
    #background_df_region["bb_m"] < 150000.0) & (background_df_region["bb_deltar"] < 2.8)]

SigYield1 = signal_df_cut["weight"].sum()
BkgYield1 = background_df_cut["weight"].sum()
SBratio = SigYield1/BkgYield1
print("signal yield in region", region, " after precut is: ", SigYield1)
print("background yield in region", region, " after precut is: ", BkgYield1)
print("S/B ratio is: ", SBratio)
dataNormal_pre = signal_df_cut.append(background_df_cut)
dataNormal_weight_df = dataNormal_pre["weight"]
if((region == 4) | (region == 2)):
    dataNormal_df = dataNormal_pre.drop(columns=["weight", "is_em", "is_me"])
if((region == 3) | (region == 1)):
    dataNormal_df = dataNormal_pre.drop(columns=["weight", "is_ee", "is_mm"])
dataNormal_df.describe()
dataNormal = dataNormal_df.values
dataNormal_weight = dataNormal_weight_df.values
mean_vec, var_vec = cuda_guass_normal.cuda_mean_var(dataNormal,dataNormal_weight)
del dataNormal
del dataNormal_df
del signalArray
del backgroundArray
del signal_df
del background_df
del dataNormal_pre

#sample_list=["sig_bbZZ.root","sig_bbWW.root","sig_bbtautau.root","data_HH.root","bkg_ttbar.root",
            #"bkg_DY.root","bkg_Higgs.root","bkg_fake.root","bkg_top.root","bkg_diboson.root"]
sample_list=["tree_sig_region4.root","tree_bkg_region4.root"]
feature_names=["bjet_0_pt","bjet_0_eta","ll_m","ll_pt","ll_deltar",
                              "met_met","met_sig","mt_lep0_met","mt_lep1_met","deltar_min_bl","deltar_max_bl","deltar_diff_bl","dphi_met_ll","dpt_met_ll","ht2","ht2r","n_central_jets","n_forward_jets","m_bbllmet","otherjet_0_pt","otherjet_0_eta","met_phi_centrality","pt_bbll_scalar","pt_bbll_vector","pt_bbllmet_scalar","pt_bbllmet_vector"]

model_list=['../NN_model_1tag/region4-1129-section0.pt', '../NN_model_1tag/region4-1129-section1.pt', '../NN_model_1tag/region4-1129-section2.pt', '../NN_model_1tag/region4-1129-section3.pt', '../NN_model_1tag/region4-1129-section4.pt']
for sample_name in sample_list:
    print("Applying ",sample_name)
    sample="/lustre/samples/di-higgs/sample-applied-1130-1tag/"+sample_name
    sigScore=bbll.apply_dnn(model_list,1, sample, "ntupe", feature_names ,"pytorch_Region4", mean_vec, var_vec)

    
    
    
    
