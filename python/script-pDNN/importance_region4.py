import sys
sys.path.insert (0,'..')
import bbll

from ROOT import TMVA, TFile, TTree, TCut, TString
#DNN Library Pytorch
import torch
from torch.utils.data import DataLoader, TensorDataset
from torch import Tensor
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
# Optimizer SWATS 
import swats
#numpy, pandas, matplotlib
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
from importlib import reload
import pandas as pd
import random
from array import array
#ML library
import pickle
import scikitplot as skplt
import seaborn as sns
import sklearn
from sklearn import preprocessing
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.metrics import classification_report, accuracy_score
from sklearn.utils import shuffle
#XGBoost Library
import xgboost as xgb
from xgboost import XGBClassifier
from xgboost import plot_tree
#from IPython import display
#import graphviz
#Cuda Library
from numba import cuda
import pycuda.autoinit
import pycuda.driver as drv
from pycuda.compiler import SourceModule
from math import sqrt
import gc 
from scipy.stats import poisson
import multiprocessing
import time


import cuda_guass_normal
#load files
signalFile = TFile.Open("../../sample-train-0320/tree_sig.root")
backgroundFile = TFile.Open("../../sample-train-0320/tree_bkg_reweighted.root")
signalTree = signalFile.Get("ntup")
backgroundTree = backgroundFile.Get("ntup")

signalNum = signalTree.GetEntries()
backgroundNum = backgroundTree.GetEntries()
print("signal events number before cut is: ", signalNum)
print("background events number before cut is: ", backgroundNum)
weight_name="reweighted_weight"
num_fold = 5
print("This training will train DNN in",num_fold,"folds")
total_variable_names=[weight_name,"process_id","m_hh_truth","is_em","is_me","bjet_0_pt","bjet_0_eta","bjet_1_pt","bjet_1_eta","ll_m","ll_pt","ll_deltar","ll_deltaeta","ll_deltaphi","bb_m",
                              "bb_pt","bb_deltar","bb_deltaeta","bb_deltapt","met_met","met_sig","met_phi","mt_lep0_met","mt_lep1_met","m_bbll","m_bbllmet","m_hh", "ht2","ht2r"]

variable_names=[weight_name,"m_hh_truth","is_em","is_me","bjet_0_pt","bjet_0_eta","bjet_1_pt","bjet_1_eta","ll_m","ll_pt","ll_deltar","ll_deltaeta","ll_deltaphi","bb_m",
                              "bb_pt","bb_deltar","bb_deltaeta","bb_deltapt","met_met","met_sig","met_phi","mt_lep0_met","mt_lep1_met","m_bbll","m_bbllmet","m_hh", "ht2","ht2r"]

print("Loading signal and background trees, please be patient")
unused_variable=[weight_name,"is_em","is_me"]
dim = len(variable_names)-len(unused_variable)
# convert minitree to numpy arrays
signalArray=signalTree.AsMatrix(total_variable_names)

backgroundArray=backgroundTree.AsMatrix(total_variable_names)
    
print(signalArray.shape)
print("signal yield before cut is: ",sum(signalArray[:,0]))
print("background yield before cut is:",sum(backgroundArray[:,0]))



signal_df=pd.DataFrame(data=signalArray,columns=total_variable_names)
background_df=pd.DataFrame(data=backgroundArray,columns=total_variable_names)

background_df = background_df[(background_df["process_id"]!=100)]
signal_df = signal_df.drop(columns=["process_id"])
background_df = background_df.drop(columns=["process_id"])
signal_df = signal_df[(signal_df["m_hh_truth"]>0)]

#region 4 selection
signal_df_region4=signal_df[(signal_df["ll_m"]<75.0)&((signal_df["is_em"]==1)|(signal_df["is_me"]==1))]
background_df_region4=background_df[(background_df["ll_m"]<75.0)&((background_df["is_em"]==1)|(background_df["is_me"]==1))]
SigYield=signal_df_region4[weight_name].sum()
BkgYield=background_df_region4[weight_name].sum()
SBratio=SigYield/BkgYield
print("signal yield in region 4 is: ",SigYield)
print("background yield in region 4 is: ",BkgYield)
print("S/B ratio is: ",SBratio)

print("Sampling background label from signal's distribution")
result_np = bbll.sample_value(len(background_df_region4["m_hh_truth"].values), signal_df_region4, signal_df_region4,
                         "m_hh_truth","reweighted_weight")


background_df_region4_np = background_df_region4.values

background_df_region4_np[:,1]=result_np

background_df_region4 = pd.DataFrame(data=background_df_region4_np, columns=variable_names)



# pre-train cuts
signal_df_cut=signal_df_region4#[(signal_df_region4["bb_m"]>80000.0)&(signal_df_region4["bb_m"]<150000.0)&(signal_df_region4["bb_deltar"]<2.8)]
background_df_cut=background_df_region4#[(background_df_region4["bb_m"]>80000.0)&(background_df_region4["bb_m"]<150000.0)&(background_df_region4["bb_deltar"]<2.8)]
SigYield1=signal_df_cut[weight_name].sum()
BkgYield1=background_df_cut[weight_name].sum()
SBratio=SigYield1/BkgYield1
print("signal yield in region 4 after precut is: ",SigYield1)
print("background yield in region 4 after precut is: ",BkgYield1)
print("S/B ratio is: ",SBratio)
#prepare numpys used for normaliztion
dataNormal_pre=signal_df_cut.append(background_df_cut)
dataNormal_weight_df=dataNormal_pre[weight_name]
dataNormal_df=dataNormal_pre.drop(columns=[weight_name,"is_em","is_me"])
dataNormal_df.describe()

dataNormal=dataNormal_df.values
dataNormal_weight=dataNormal_weight_df.values
reload(cuda_guass_normal)
mean_vec, var_vec = cuda_guass_normal.cuda_mean_var(dataNormal,dataNormal_weight)
# Add label to signal and background
signal_df_cut["label"]=1
background_df_cut["label"]=0

# Drop negative weighted events to import stability 
signal_df_pos = signal_df_cut[signal_df_cut[weight_name]>0]
background_df_pos = background_df_cut[background_df_cut[weight_name]>0]

signal_df_pos=shuffle(signal_df_pos)
background_df_pos = shuffle(background_df_pos)

signal_df_pos=shuffle(signal_df_pos)
background_df_pos = shuffle(background_df_pos)
# count signal and background size


del dataNormal
del dataNormal_df
del signalArray
del backgroundArray

#n_fold training
batch_size = 20000
max_epoch=300
patient=4
drop_variable =[weight_name, 'label', 'is_em', 'is_me']
model_name="../NN_model_pDNN/temp/region4-0419-section"
device = torch.device("cuda:0")
isRes=0


training_varname = ["m_hh_truth","bjet_0_pt","bjet_0_eta","bjet_1_pt","bjet_1_eta","ll_m","ll_pt","ll_deltar","ll_deltaeta","ll_deltaphi","bb_m",
                              "bb_pt","bb_deltar","bb_deltaeta","bb_deltapt","met_met","met_sig","met_phi","mt_lep0_met","mt_lep1_met","m_bbll","m_bbllmet","m_hh", "ht2","ht2r"]
tag_num = training_varname.index("m_hh_truth")
mass_points= [251, 260, 300, 400, 500,600, 800, 1000]
mass_points_string = ["251","260","300","400","500","600","800","1000"]
num_mass_points = len(mass_points)
variable_num=len(training_varname)
train_auc_np=np.zeros((variable_num,num_mass_points))
test_auc_np=np.zeros((variable_num,num_mass_points))
train_auc_np_total = np.zeros((variable_num,1))
test_auc_np_total = np.zeros((variable_num,1))

nominal_auc_train, nominal_auc_test, nominal_auc_train_total, nominal_auc_test_total = bbll.calculate_importance_pDNN(num_fold, dim, signal_df_pos, background_df_pos, mean_vec, var_vec, isRes, device, model_name, drop_variable, weight_name, batch_size, max_epoch, patient,0,0,tag_num,mass_points)

count_num=0

for test_varname in training_varname:
    print("Calculating importance of variable: ",test_varname)
    test_auc_train, test_auc_test, test_auc_train_total, test_auc_test_total = bbll.calculate_importance_pDNN(num_fold, dim, signal_df_pos, background_df_pos, mean_vec, var_vec, isRes, device, model_name, drop_variable, weight_name, batch_size, max_epoch, patient,1,count_num,tag_num,mass_points)
    train_auc_np[count_num,:] = nominal_auc_train-test_auc_train
    test_auc_np[count_num,:] = nominal_auc_test-test_auc_test
    train_auc_np_total[count_num] = nominal_auc_train_total-test_auc_train_total
    test_auc_np_total[count_num] = nominal_auc_test_total-test_auc_test_total
    print("Train auc change of variable ", test_varname," is:",train_auc_np_total[count_num], " Test auc change of variable ", test_varname," is:",test_auc_np_total[count_num])
    count_num=count_num+1
xlabel="Feature_Score"
count_n = 0    
for masspoint in mass_points_string:
    plot_title = "Input feature importance (by Test AUC,LM/DF region,"+masspoint+"GeV)"
    save_path = '../../figures/0419/ranking-region4-gain-pDNN-'+masspoint+'.pdf'
    temp_varname = training_varname.copy()
    temp_np = test_auc_np[:,count_n].copy()
    bbll.get_ranking_plot(temp_np,temp_varname,plot_title,xlabel,save_path)
    count_n=count_n+1

plot_title = "Input feature importance (by Test AUC,LM/DF region)"
save_path = '../../figures/0419/ranking-region4-gain-pDNN.pdf'
temp_varname = training_varname.copy()
bbll.get_ranking_plot(test_auc_np_total,temp_varname,plot_title,xlabel,save_path)

count_n = 0
plot_title = "Test AUC change at different mass points [GeV]"
for variable_name in training_varname:
    xlabel=variable_name
    save_path = '../../figures/0419/ranking-region4-gain-pDNN-'+variable_name+'.pdf'
    temp_varname = mass_points_string.copy()
    temp_np = test_auc_np[count_n,:].copy()
    bbll.get_ranking_plot(temp_np,temp_varname,plot_title,xlabel,save_path)
    count_n=count_n+1
    
print("Finishing calculate importance")


