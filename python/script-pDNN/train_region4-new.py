import sys
sys.path.insert (0,'..')
import bbll
import multiprocessing


weight_name = "reweighted_weight"
num_fold = 5
variable_names=[weight_name,"process_id","m_hh_truth","is_em","is_me","bjet_0_pt","bjet_0_eta","bjet_1_pt","bjet_1_eta","ll_m","ll_pt","ll_deltar","ll_deltaeta","ll_deltaphi","bb_m",
                              "bb_pt","bb_deltar","bb_deltaeta","bb_deltapt","met_met","met_sig","met_phi","mt_lep0_met","mt_lep1_met","m_bbll","m_bbllmet","m_hh", "ht2","ht2r"]

unused_variable=[weight_name,"is_em","is_me",'process_id']
dim = len(variable_names)-len(unused_variable)

parameter_list = {}
parameter_list["sample_path"] = "../../sample-train-0320"
parameter_list["sample_name1"] = "tree_sig.root"
#parameter_list["sample_name2"] = "tree_bkg_reweighted.root"
parameter_list["sample_name2"] = "tree_bkg_top.root"
parameter_list["sample_name3"] = "tree_bkg_others.root"
parameter_list["ntuple_name"] = "ntup"
parameter_list["sample_num"] = 3 
parameter_list["variable_list"] = variable_names 
parameter_list["weight_name"] = weight_name
parameter_list["unused_variable"] = unused_variable
parameter_list["label_list"] = [0,1,2]
parameter_list["do_reweight"] = True
parameter_list["cut_num"] = 2
parameter_list["cut1_type"] = "part"
parameter_list["cut1"] = '(DF["m_hh_truth"]>0)'
parameter_list["cut1_samples"] = [1]
parameter_list["cut2_type"] = "all"
parameter_list["cut2"] = '(DF["ll_m"]<75.0)&((DF["is_em"]==1)|(DF["is_me"]==1))'
parameter_list["ispDNN"] = True
parameter_list["tag_variable"] = "m_hh_truth"
parameter_list["target_sample"] = [1]
parameter_list["sample_to_fill"] = [2,3]
parameter_list["get_correlation_matrix"] = True
parameter_list["figures_path"] = "../../figures/0430"
parameter_list["do_shuffle"] = True
parameter_list["ratio_list"] = [1,1,1]

out_list = bbll.create_dataset(parameter_list)



batch_size = 20000
max_epoch=300
patient=4
drop_variable =[weight_name, 'label', 'is_em', 'is_me','process_id']
model_name="../NN_model_pDNN/region4-0430-section"
net_parameter = {}
optim_para = {}
optim_para["type"] = "swats"
optim_para["lr"] = 1e-3
optim_para["beta"] = 0.9
net_parameter["num_cpu"] = multiprocessing.cpu_count()-2
net_parameter["optimizer"] = optim_para
net_parameter["input_size"] = dim
net_parameter["device"] = "gpu"
net_parameter["model_type"]=3
net_parameter["dim_list"] = [dim, 100,200,300,200,300]
net_parameter["drop_out"] = [False, True, True,True,True,True]
net_parameter["drop_out_rate"] = [0.2,0.2,0.2,0.2,0.2,0.2]
res_param = {}
res_param["input_dim"] =dim
res_param["Res_dim"] = 100
res_param["num_res"] = 4
res_param["Res_depth"] = 5
res_param["dropout_rate"]=0.0
net_parameter["Res_parameters"] = res_param
net_parameter["num_class"] = out_list["num_class"]


bbll.train_n_fold_new(num_fold, out_list, net_parameter, model_name, drop_variable, weight_name, batch_size, max_epoch, patient)






