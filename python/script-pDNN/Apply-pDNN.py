import sys
sys.path.insert (0,'..')
import bbll

from ROOT import TMVA, TFile, TTree, TCut, TString
#DNN Library Pytorch
import torch
from torch.utils.data import DataLoader, TensorDataset
from torch import Tensor
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
# Optimizer SWATS 
import swats
#numpy, pandas, matplotlib
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
from importlib import reload
import pandas as pd
import random
from array import array
#ML library
import pickle
import scikitplot as skplt
import seaborn as sns
import sklearn
from sklearn import preprocessing
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.metrics import classification_report, accuracy_score
from sklearn.utils import shuffle
#XGBoost Library
import xgboost as xgb
from xgboost import XGBClassifier
from xgboost import plot_tree
#from IPython import display
#import graphviz
#Cuda Library
from numba import cuda
import pycuda.autoinit
import pycuda.driver as drv
from pycuda.compiler import SourceModule
from math import sqrt
import gc 
from scipy.stats import poisson
import multiprocessing
import time
import cuda_guass_normal

#Load tree for input feature normalization
signalFile = TFile.Open("../../sample-train-0313/tree_sig.root")
backgroundFile = TFile.Open("../../sample-train-0313/tree_bkg_reweighted.root")
signalTree = signalFile.Get("ntup")
backgroundTree = backgroundFile.Get("ntup")
signalNum = signalTree.GetEntries()
backgroundNum = backgroundTree.GetEntries()
print("signal events number before cut is: ", signalNum)
print("background events number before cut is: ", backgroundNum)
# Set region

region=4

weight_name="reweighted_weight"

if((region == 2) | (region ==4)):
    sig_variable_names=[weight_name,"process_id","m_hh_truth","is_em","is_me","bjet_0_pt","bjet_0_eta","bjet_1_pt","bjet_1_eta","ll_m","ll_pt","ll_deltar","ll_deltaeta","ll_deltaphi","bb_m",
                              "bb_pt","bb_deltar","bb_deltaeta","bb_deltapt","met_met","met_sig","met_phi","mt_lep0_met","mt_lep1_met","m_bbll","m_bbllmet","m_hh","ht2","ht2r"]
    variable_names=[weight_name,"m_hh_truth","is_em","is_me","bjet_0_pt","bjet_0_eta","bjet_1_pt","bjet_1_eta","ll_m","ll_pt","ll_deltar","ll_deltaeta","ll_deltaphi","bb_m",
                              "bb_pt","bb_deltar","bb_deltaeta","bb_deltapt","met_met","met_sig","met_phi","mt_lep0_met","mt_lep1_met","m_bbll","m_bbllmet","m_hh","ht2","ht2r"]
if((region == 1) | (region ==3)):
    sig_variable_names=[weight_name,"process_id","m_hh_truth","is_ee","is_mm","bjet_0_pt","bjet_0_eta","bjet_1_pt","bjet_1_eta","ll_m","ll_pt","ll_deltar","ll_deltaeta","ll_deltaphi","bb_m",
                              "bb_pt","bb_deltar","bb_deltaeta","bb_deltapt","met_met","met_sig","met_phi","mt_lep0_met","mt_lep1_met","m_bbll","m_bbllmet","m_hh","ht2","ht2r"]
    variable_names=[weight_name,"m_hh_truth","is_ee","is_mm","bjet_0_pt","bjet_0_eta","bjet_1_pt","bjet_1_eta","ll_m","ll_pt","ll_deltar","ll_deltaeta","ll_deltaphi","bb_m",
                              "bb_pt","bb_deltar","bb_deltaeta","bb_deltapt","met_met","met_sig","met_phi","mt_lep0_met","mt_lep1_met","m_bbll","m_bbllmet","m_hh","ht2","ht2r"]
    
unused_variable=[weight_name,"is_em","is_me"]
dim = len(variable_names)-len(unused_variable)

signalArray = signalTree.AsMatrix(sig_variable_names)
backgroundArray = backgroundTree.AsMatrix(sig_variable_names)
signal_df = pd.DataFrame(data=signalArray, columns=sig_variable_names)
background_df = pd.DataFrame(data=backgroundArray, columns=sig_variable_names)
background_df = background_df[(background_df["process_id"]!=100)]
signal_df = signal_df.drop(columns=["process_id"])
background_df = background_df.drop(columns=["process_id"])

signal_df = signal_df[(signal_df["m_hh_truth"]>0)]



if(region == 1):
    signal_df_region = signal_df[(signal_df["ll_m"] > 75.0) & (
        (signal_df["is_ee"] == 1) | (signal_df["is_mm"] == 1))]
    background_df_region = background_df[(background_df["ll_m"] > 75.0) & (
        (background_df["is_ee"] == 1) | (background_df["is_mm"] == 1))]
if(region == 2):
    signal_df_region = signal_df[(signal_df["ll_m"] > 75.0) & (
        (signal_df["is_em"] == 1) | (signal_df["is_me"] == 1))]
    background_df_region = background_df[(background_df["ll_m"] > 75.0) & (
        (background_df["is_em"] == 1) | (background_df["is_me"] == 1))]

if(region == 3):
    signal_df_region = signal_df[(signal_df["ll_m"] < 75.0) & (
        (signal_df["is_ee"] == 1) | (signal_df["is_mm"] == 1))]
    background_df_region = background_df[(background_df["ll_m"] < 75.0) & (
        (background_df["is_ee"] == 1) | (background_df["is_mm"] == 1))]
if(region == 4):
    signal_df_region = signal_df[(signal_df["ll_m"] < 75.0) & (
        (signal_df["is_em"] == 1) | (signal_df["is_me"] == 1))]
    background_df_region = background_df[(background_df["ll_m"] < 75.0) & (
        (background_df["is_em"] == 1) | (background_df["is_me"] == 1))]

signal_df_cut = signal_df_region#[(signal_df_region["bb_m"] > 80000.0) & (
    #signal_df_region["bb_m"] < 150000.0) & (signal_df_region["bb_deltar"] < 2.8)]
background_df_cut = background_df_region#[(background_df_region["bb_m"] > 80000.0) & (
    #background_df_region["bb_m"] < 150000.0) & (background_df_region["bb_deltar"] < 2.8)]

SigYield1 = signal_df_cut[weight_name].sum()
BkgYield1 = background_df_cut[weight_name].sum()
SBratio = SigYield1/BkgYield1
print("signal yield in region", region, " after precut is: ", SigYield1)
print("background yield in region", region, " after precut is: ", BkgYield1)
print("S/B ratio is: ", SBratio)


result_np = bbll.sample_value(len(background_df_cut["m_hh_truth"].values), signal_df_cut, signal_df_cut,
                         "m_hh_truth",weight_name)


background_df_cut_np = background_df_cut.values

background_df_cut_np[:,1]=result_np

background_df_cut = pd.DataFrame(data=background_df_cut_np, columns=variable_names)


dataNormal_pre = signal_df_cut.append(background_df_cut)
dataNormal_weight_df = dataNormal_pre[weight_name]
if((region == 4) | (region == 2)):
    dataNormal_df = dataNormal_pre.drop(columns=[weight_name, "is_em", "is_me"])
if((region == 3) | (region == 1)):
    dataNormal_df = dataNormal_pre.drop(columns=[weight_name, "is_ee", "is_mm"])
dataNormal_df.describe()
dataNormal = dataNormal_df.values
dataNormal_weight = dataNormal_weight_df.values
reload(cuda_guass_normal)
mean_vec, var_vec = cuda_guass_normal.cuda_mean_var(dataNormal,dataNormal_weight)
del dataNormal
del dataNormal_df
del signalArray
del backgroundArray

sample_list=["bkg_top.root","bkg_DY.root","bkg_fakes.root","bkg_diboson.root","bkg_Higgs.root","tree_sig_res.root","data.root"]
fill_list=[1,1,1,1,1,0,1]
#sample_list=["tree_bkg_2tag.root","tree_sig_2tag.root"]
feature_names=["m_hh_truth","bjet_0_pt","bjet_0_eta","bjet_1_pt","bjet_1_eta","ll_m","ll_pt","ll_deltar","ll_deltaeta","ll_deltaphi","bb_m",
                              "bb_pt","bb_deltar","bb_deltaeta","bb_deltapt","met_met","met_sig","met_phi","mt_lep0_met","mt_lep1_met","m_bbll","m_bbllmet","m_hh","ht2","ht2r"]
model_suffix='../NN_model_pDNN/region4-0319-section'
model_list=[model_suffix+"0.pt",model_suffix+"1.pt",model_suffix+"2.pt",model_suffix+"3.pt",model_suffix+"4.pt"]
mass_point = [251, 260, 300, 400, 500,600, 800, 1000]
mass_name = ['251','260','300','400','500','600','800','1000']
isRes=1

treename = 'ntup'
parameter_name = 'm_hh_truth'
for (sample_name, reFill) in zip(sample_list, fill_list): 
    print("Applying to sample:",sample_name)
    for (mass, name) in zip(mass_point, mass_name):
        print("Applying mass point",name)
        if(reFill==1):
            print("Fill variable",parameter_name,"with value",mass)
        sample="/lustre/samples/di-higgs/sample-applied-0319/"+sample_name
        branchname='pytorch_Region4_pDNN_'+name
        sigScore=bbll.apply_pdnn(model_list, isRes, sample, treename, feature_names, branchname, mean_vec, var_vec,reFill, parameter_name, mass)
 
    
    
    
    
