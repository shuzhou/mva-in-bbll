import numpy as np 
import random
from collections import deque 

import torch
from torch.utils.data import DataLoader, TensorDataset
from torch import Tensor
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

import swats 



class Mish(nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, x):
        return x *( torch.tanh(F.softplus(x)))



def init_weights(m):
    if type(m) == nn.Linear or type(m) == nn.Conv2d:
        torch.nn.init.xavier_uniform_(m.weight)

class Flatten(torch.nn.Module):
    def forward(self, x):
        return x.view(x.shape[0], -1)

class Net(nn.Module):
    
    def __init__(self,inpu_dim,output_dim):
        super().__init__()
        self.fc1 = nn.utils.weight_norm(nn.Linear(inpu_dim, 32),name='weight')
        self.mish = Mish()
        self.fc2 = nn.utils.weight_norm(nn.Linear(32, 16),name='weight')
        self.fc3 = nn.utils.weight_norm(nn.Linear(16, 8),name='weight')
        self.fc4 = nn.utils.weight_norm(nn.Linear(8,16),name='weight')
        self.out = nn.utils.weight_norm(nn.Linear(16, output_dim),name='weight')
        #self.out_act = nn.Sigmoid()
        
    def forward(self, input_):
        a1 = self.fc1(input_)
        m1 = self.mish(a1)
        l2 = self.fc2(m1)
        m2 = self.mish(l2)
        l3 = self.fc3(m2)
        m3 = self.mish(l3)
        l4 = self.fc4(m3)
        m4 = self.mish(l4)
        y = self.out(m4)
        return y
    
def train(model, device, train_loader, loss_function, optimizer, epoch):
    model.train()
    average_loss=0
    batch_n=0
    for batch_idx, (data, target) in enumerate(train_loader):
        batch_n=batch_n+1
        #data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        data=data.squeeze()
        output = model(data)
        loss = loss_function(output, target)
        loss.backward()
        average_loss=average_loss+loss.item()
        optimizer.step()
        torch.cuda.empty_cache()
    return(average_loss/batch_n)




def eval_pytorch(model,input_loader):
    i=0
    model.eval()
    with torch.no_grad():
        for batch_idx, (data) in enumerate(input_loader):
            data = data
            score=model(data)
            del data
            score_np=score.data.cpu().numpy()
            if(i==0):
                output_numpy=score_np
            if(i!=0):
                output_numpy=np.vstack((output_numpy,score_np))
            i=i+1
            torch.cuda.empty_cache()
    return(output_numpy)


def predict_score(model, device, states):
    inputTensor = torch.tensor(states, dtype=torch.float,device=device)
    inputLoader = DataLoader(inputTensor,batch_size=100, shuffle=False)
    score = eval_pytorch(model, inputLoader)
    return(score)

def train_model(model, device, states, target, loss_function, optimizer, batch, num_epoch):
    stateTensor = torch.tensor(states, dtype=torch.float,device=device)
    targetTensor = torch.tensor(target, dtype=torch.float,device=device)
    TrainDataset = TensorDataset(stateTensor, targetTensor)
    TrainLoader = DataLoader(TrainDataset, batch_size=batch, shuffle=False)
    for i in range(num_epoch):
        loss = train(model, device, TrainLoader, loss_function, optimizer, i)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    