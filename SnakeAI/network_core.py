import time 
import random 
import numpy as np 
from collections import deque

from pytorch_network import QNetwork 
from memory import ReplayMemory 
import torch 


class Network_core:
    def __init__(self, env, learning_rate = 0.001, batch_size = 64, gamma = 1.0):
        self.env = env
        self.batch_size = batch_size 
        self.gamma = gamma 
        self.lr = learning_rate 
        self.memory_cap = int(1e5)
        self.output_size = env.ACTION_SPACE
        self.input_size = env.STATE_SPACE
        
        self.network = QNetwork(input_shape = self.input_size, output_size = self.output_size, learning_rate = self.lr)
        #init DQN network 
        self.memory = ReplayMemory(self.memory_cap, self.batch_size)
        #init memory
        self.t = 0
        
    def add_state(self, state, action, reward, next_state, done):
        self.memory.add(state, action, reward, next_state, done)
        #push new state information to memory
    def save(self, model_num, directory):
        #save trained network model
        save_name = directory + '/pytorch_snake_' + model_num + '.pt'
        print("model saved to ",save_name)
        torch.save(self.network.model.state_dict(), save_name)
    
    def new_action(self, state, epsilon = 0):
        #get new action from network
        state = state.reshape((1,)+state.shape)
        best_action = self.network.predict(state)
        #best action from network
        
        if random.random() > epsilon:
            next_action = np.argmax(best_action)
            #choose action from DQN network
        else:
            next_action = random.randint(0, self.output_size-1)
            #random action
            
        return(next_action)
    
    def train_network(self):
        if self.memory.__len__() > self.batch_size:
            # random sample states and actions from saved memory
            states, actions, rewards, next_states, dones = self.memory.sample(self.env.STATE_SPACE)
            #init 4-dim target value from current network
            target = self.network.predict(states, self.batch_size)
            # calculate each action's reward from current network
            next_score = self.network.predict(next_states, self.batch_size)
            # get best next action for each state 
            best_action = np.argmax(next_score, axis = 1)
            # then correct target value from true action
            for i in range(self.batch_size):
                if dones[i]:
                    #if no next action, the rewards just the rewards gotten in this action
                    target[i][actions[i]] = rewards[i]
                    
                else:
                    #if next step exits, the rewards need to consider best rewards next step could get 
                    target[i][actions[i]] = rewards[i] + self.gamma*next_score[i][best_action[i]]
                    
            #train the network 
            
            self.network.train(states, target, batch_size = self.batch_size)
            
            
            
            
            
            
            
            
        
        
        
    