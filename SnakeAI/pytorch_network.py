import numpy as np 
import random
from collections import deque 

import torch
from torch.utils.data import DataLoader, TensorDataset
from torch import Tensor
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

from pytorch import * 


device = torch.device("cuda:0")



class QNetwork:

    def __init__(self,input_shape, output_size, learning_rate=0.01):
        self.input_shape = input_shape
        self.output_size = output_size
        self.lr = learning_rate 
        self.loss_function = nn.MSELoss().cuda()
        self.model, self.optimizer= self._build_model()
        
    def _build_model(self):
        model = Net(self.input_shape, self.output_size).to(device)
        model.apply(init_weights)
        optimizer = optim.Adam(model.parameters(),lr=self.lr)
        return(model, optimizer)
        
        


    def predict(self, state, batch_size=1):
        score = predict_score(self.model, device, state)
        return(score)
    
    def train(self, states, action_values, batch_size):
        train_model(self.model, device, states, action_values, self.loss_function, self.optimizer, batch_size, 4)