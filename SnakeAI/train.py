import sys
from network_core import Network_core
from Environment import Snake_Env
from collections import deque
import numpy as np



WHITE = (255, 255, 255)
GREEN = (0, 0, 255)

model_out_dir = 'torch_models'
# complicated model
model_arch = 'davinci'
resume_model_path = ''

max_env_width, max_env_height = 50, 50
env_width, env_height = 5, 5
display_width, display_height = 900, 900  # size of display
env = Snake_Env(max_env_width, max_env_height, env_width,
                env_height, display_width, display_height)

# params needs to finetune
HIDDEN_UNITS = (32, 16, 32)
NETWORK_LR = 0.001
BATCH_SIZE = 64
UPDATE_EVERY = 5
GAMMA = 0.95
NUM_EPISODES = 50000



def train():
    print("Welcom to AI snake game!")
    print("Our goal is to train AI to play snake gamme, we will train", NUM_EPISODES,"games. This will take about two days")

    core = Network_core(env, learning_rate = NETWORK_LR, batch_size = BATCH_SIZE, gamma = GAMMA)
    
   
    #if resume_model_path and resume_model_path != '':
        #logging.info('model resumed from: {}'.format(resume_model_path))
        #agent.qnetwork_local.model = load_model(resume_model_path)
    #else:
        #logging.info('start training from scratch...')

    # list containing scores from each episode
    scores, avg_scores = [], []
    INCREASE_EVERY, SAVE_EVERY = 500, 500
    scores_window = deque(maxlen=INCREASE_EVERY)
    stats = [0, 0, 0, 0, 0]

    epsilon = 1
    eps_min = 0.05 
    eps_decay = 0.9997
    
    for i_episode in range(1, NUM_EPISODES+1):
        epsilon = max(epsilon*eps_decay, eps_min)
        state = env.reset()
        action = core.new_action(state, epsilon)
        # render the environment
        env.render(action, stats, i_episode, epsilon, GAMMA)
        score = 0
        while True:
            next_state, reward, done, info = env.step(action)
            stats[info] += 1  # collecting some stats
            # add the experience to agent's memory
            core.add_state(state, action, reward, next_state, done)
            # let's teach our agent to do something! hopefully they learn.
            core.train_network()
            # render the environment
            env.render(action, stats, i_episode, epsilon, GAMMA)
            if done:
                # finish this episode
                break
            # update state and action
            state = next_state
            action = core.new_action(state, epsilon)
            score += reward
        scores_window.append(score)       # save most recent score
        scores.append(score)              # save most recent score
        avg_scores.append(np.mean(scores_window))

        if (i_episode + 1) % SAVE_EVERY == 0:
            core.save('{}_{}'.format(model_arch, i_episode+1), model_out_dir)  # save the model
            print('Episode {}, Score {}, Average Score: {:.2f}'.format(
                i_episode+1, score, np.mean(scores_window)))
        # increase environment size
        if (i_episode + 1) % INCREASE_EVERY == 0:
            env.change_size(1, 1)  # increase the env size by 1
        # after 6k episodes increase up the training process
        if (i_episode + 1) == 6000:
            INCREASE_EVERY = 100  # That is increase the size by 1, every 100 episodes
            SAVE_EVERY = 100
    # save the agent's q-network for testing
    core.save('{}_final'.format(model_arch), model_out_dir)


if __name__ == "__main__":
    train()
