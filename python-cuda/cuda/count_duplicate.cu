#include <stdio.h>
#include <math.h>
#include <cstdlib>
#include "count_duplicate.cuh"
//input need to be sorted array
__global__ void gpu_count_duplicate(float *input_vec, int *output_vec, long long size){
	extern __shared__ int counts[];
	int tid=threadIdx.x;
	int stride=gridDim.x*blockDim.x;
	int index=blockIdx.x*blockDim.x+threadIdx.x;
	counts[tid]=0;
	__syncthreads();
	for(int i=index;i<size-1;i=i+stride){
		if(input_vec[i]==input_vec[i+1]){
			counts[tid]++;
		}
	}
	__syncthreads();
	for(int offset=blockDim.x/2;offset>0;offset>>=1){
		if(tid<offset){
			counts[tid]=counts[tid]+counts[tid+offset];
		}
		__syncthreads();

	}
	if(tid==0){
		output_vec[blockIdx.x]=counts[tid];
	}
}


int count_duplicate(float *input_vec, long long size){
	float *a;
	int *out;
	int deviceId;
	int numberOfSMs;
	int count=0;
	const int num_threads=1024;
	int num_blocks;
	cudaGetDevice(&deviceId);
	cudaDeviceGetAttribute(&numberOfSMs, cudaDevAttrMultiProcessorCount, deviceId);
	num_blocks=numberOfSMs;
	dim3 block_num(num_blocks,1,1);
	dim3 threads_per_block(num_threads,1,1);
	cudaMallocManaged(&a,size*sizeof(float));
	cudaMemcpy(a, input_vec, size*sizeof(float),cudaMemcpyHostToDevice);
	cudaMallocManaged(&out, num_blocks*sizeof(int));
	cudaMemPrefetchAsync(out, num_blocks*sizeof(int), deviceId);
	gpu_count_duplicate<<<block_num,threads_per_block,num_threads*sizeof(float)>>>(a, out, size);
	cudaDeviceSynchronize();
	cudaMemPrefetchAsync(out, sizeof(out), cudaCpuDeviceId);
	for(int i=0;i<num_blocks;i++){
		count=count+out[i];
	}
	return(count);
}
