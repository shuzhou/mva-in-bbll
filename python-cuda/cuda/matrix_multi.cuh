#ifndef MATRIX_MULTI       
#define MATRIX_MULTI

#include <stdio.h>

extern "C" 

void matrix_multi(float *a, float *b, float *c, int a_col, int a_row, int b_col, int b_row);
    
#endif
