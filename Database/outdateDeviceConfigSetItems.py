#!/usr/bin/env python

import sys, os, shutil, time
import MDTConfigDBUtils
import cx_Oracle

def exitWithStatus(dbInstance, status, statusStr, inputs):
  w = 10
  if len(dbInstance) > w: w = len(dbInstance)
  if status == 0:
    print "%*s : outdate of <%s %s> COMPLETE" % (w, dbInstance, inputs.deviceName, inputs.deviceConfig)
    print "%*s : %s" % (w, dbInstance, statusStr)
  else:
    print "%*s : FAILED to outdate config <%s %s %s> [Status = %i]" % (w, 
                                                                   dbInstance, 
                                                                   inputs.deviceName, 
                                                                   inputs.deviceConfig, 
                                                                   inputs.configSet, 
                                                                   int(status))
    print "%*s  Cause of failure: %s" % (w+1, " " , statusStr)
 
  sys.exit(status)

def outdateDeviceConfigSetItems(dbInstance, inputs, status = ''):
  dbInstance = dbInstance.lower()
  user, passwd, tns, acct = MDTConfigDBUtils.getDBCredentials(dbInstance)
  try: 
    connection = cx_Oracle.connect(user, passwd, tns)
  except cx_Oracle.DatabaseError, exc:
    print "| ERROR Connection problem"
    error, = exc.args
    print "| ----- Oracle Error Code", error.code
    print "| ----- Oracle Error Message", error.message
    sys.exit(3)
  cursor = connection.cursor()
  # ------------------------------------------------------------------
  # some setup
  if inputs.configSet == 'Default':
    inputs.configSet = 'Default Set'
  #configName = inputs.deviceType
  #configSet = configName + " Set"
  # ------------------------------------------------------------------
  # Let's check if this configuration set is in the DB
  queryBase = 'select count(*)\n' \
              'from {0}.config_sets c\n' \
              'where c.set_name = \'{1}\'\n'.format(acct, inputs.configSet)
  try: 
    results = cursor.execute(queryBase).fetchall()
  except cx_Oracle.DatabaseError, exc:
    print "| ERROR Query problem"
    error, = exc.args
    print "| ----- Query"
    print queryBase
    print "| ----- Oracle Error Code", error.code
    print "| ----- Oracle Error Message", error.message
    sys.exit(3)
  count = results[0][0]
  if count == 0:
    exitWithStatus(dbInstance, 10, "No configuration set <%s> in MDT configuration DB" % inputs.configSet, inputs)
  elif count > 1:
    exitWithStatus(dbInstance, 20, "Too many configuration sets with name <%s> in MDT configuration DB" % inputs.configSet, inputs)
  # ------------------------------------------------------------------
  # Let's check if this device is already in the DEVICES table
  queryBase = 'select count(*)\n' \
              'from {0}.devices d\n' \
              'where d.device_name = \'{1}\'\n'.format(acct, inputs.deviceName)
  try: 
    results = cursor.execute(queryBase).fetchall()
  except cx_Oracle.DatabaseError, exc:
    print "| ERROR Query problem"
    error, = exc.args
    print "| ----- Query"
    print queryBase
    print "| ----- Oracle Error Code", error.code
    print "| ----- Oracle Error Message", error.message
    sys.exit(3)
  count = results[0][0]
  if count == 0:
    exitWithStatus(dbInstance, 11, "Cannot find device <%s> in MDT configuration DB" % inputs.deviceName, inputs)
  elif count > 1:
    exitWithStatus(dbInstance, 21, "Too many devices with name <%s> in MDT configuration DB" % inputs.deviceName, inputs)
  # ------------------------------------------------------------------
  # Let's check if this device is already in the DEVICE_CONFIGS table
  queryBase = 'select count(*)\n' \
              'from {0}.devices d, \n' \
              '     {0}.device_configs c\n' \
              'where d.device_name = \'{1}\'\n' \
              '  and c.config_name = \'{2}\'\n' \
              '  and d.device_oid = c.device_id\n'.format(acct, inputs.deviceName, inputs.deviceConfig)
  try: 
    results = cursor.execute(queryBase).fetchall()
  except cx_Oracle.DatabaseError, exc:
    print "| ERROR Query problem"
    error, = exc.args
    print "| ----- Query"
    print queryBase
    print "| ----- Oracle Error Code", error.code
    print "| ----- Oracle Error Message", error.message
    sys.exit(3)
  count = results[0][0]
  if count == 0:
    exitWithStatus(dbInstance, 12, "Cannot find device config  <%s> in MDT configuration DB" % inputs.deviceConfig, inputs)
  if count > 1:
    exitWithStatus(dbInstance, 22, "Too many device configs <%s> in MDT configuration DB" % inputs.deviceConfig, inputs)

  # ------------------------------------------------------------------
  # Let's check for valid config items in the CONFIG_SET_ITEMS table
  queryBase = 'select count(*)\n' \
              'from {0}.devices d, \n' \
              '     {0}.device_configs c,\n' \
              '     {0}.config_set_items i\n' \
              'where d.device_name = \'{1}\'\n' \
              '  and c.config_name = \'{2}\'\n' \
              '  and c.config_oid = i.config_id\n' \
              '  and d.device_oid = c.device_id\n' \
              '  and i.since < sysdate\n' \
              '  and i.until > sysdate\n'.format(acct, inputs.deviceName, inputs.deviceConfig)
  
  try: 
    results = cursor.execute(queryBase).fetchall()
  except cx_Oracle.DatabaseError, exc:
    print "| ERROR Query problem"
    error, = exc.args
    print "| ----- Query"
    print queryBase
    print "| ----- Oracle Error Code", error.code
    print "| ----- Oracle Error Message", error.message
    sys.exit(3)
  count = results[0][0]
  print count
  if count == 0:
    exitWithStatus(dbInstance, 0, "no Config set item <%s %s> exists in MDT configuration DB, NO CHANGE" % (inputs.deviceName, inputs.deviceConfig), inputs)
  if count > 1:
    exitWithStatus(dbInstance, 22, "Too many config set items <%s %s> in MDT configuration DB" % (inputs.deviceName, inputs.deviceConfig), inputs)

  #
  # outdate selected config set item
  queryBase = 'update {0}.config_set_items \n' \
              'set until = sysdate\n'\
              'where item_oid in \n'\
              '(\n' \
              'select i.item_oid \n'\
              'from {0}.devices d, \n' \
              '     {0}.device_configs c,\n' \
              '     {0}.config_set_items i\n' \
              'where d.device_name = \'{1}\'\n' \
              '  and c.config_name = \'{2}\'\n' \
              '  and c.config_oid = i.config_id\n' \
              '  and d.device_oid = c.device_id\n' \
              '  and i.since < sysdate\n' \
              '  and i.until > sysdate)\n'.format(acct, inputs.deviceName, inputs.deviceConfig)
  #queryBase = 'select i.item_oid\n' \
              #'from {0}.devices d, \n' \
             # '     {0}.device_configs c,\n' \
             ## 'where d.device_name = \'{1}\'\n' \
             # '  and c.config_name = \'{2}\'\n' \
             # '  and c.config_oid = i.config_id\n' \
             # '  and d.device_oid = c.device_id\n' \
             #'  and i.since < sysdate\n' \
             # '  and i.until > sysdate\n'.format(acct, inputs.deviceName, inputs.deviceConfig)
  #print queryBase
  try: 
    cursor.execute(queryBase)
    #results = cursor.execute(queryBase).fetchall()
    #print results[0]
  except cx_Oracle.DatabaseError, exc:
    print "| ERROR Query problem"
    error, = exc.args
    print "| ----- Query"
    print queryBase
    print "| ----- Oracle Error Code", error.code
    print "| ----- Oracle Error Message", error.message
    sys.exit(3)
  connection.commit()
  time.sleep(2)
  exitWithStatus(dbInstance, 0, 'All OK', inputs)
  

    
if __name__ == "__main__":
  print "%30s: outdate configure set items into MDT configuration DB config_set_items table (ATONR and DEVDB11)." % sys.argv[0]
  if len(sys.argv) < 3:
    print "%30s: %s <deviceName> <deviceConfigName> <configSetName>" % ("Usage", sys.argv[0])
    print "%30s: device name"                          % "deviceName"
    print "%30s: device config name, e.g., 'Default'"  % "deviceConfigName"
    print "%30s: config set name, e.g., 'Default Set'" % "configSetName"
    sys.exit(1)
  deviceName       = sys.argv[1]
  deviceConfigName = sys.argv[2]
  configSetName    = sys.argv[3]
  if not MDTConfigDBUtils.isAllowedDomain():
    print ""
    print "| ERROR You are not logged in to an appropriate machine to insert a new device configuration in the"
    print "| ----- MDT configuration DB. Please log in to a node at Point 1 to make changes."
    print ""
    sys.exit(1)
  if not MDTConfigDBUtils.isAllowedUser():
    print ""
    print "| ERROR You are not allowed to insert a new device configuration in the"
    print "| ----- MDT configuration DB. Please contact Tiesheng.Dai@cern.ch or Devin.Harper@cern.ch to "
    print "| ----- request permission (include name , institute, and contact person)."
    print ""
    sys.exit(2)
  emailAddress   = MDTConfigDBUtils.getEmailAddressForUser()
  emailAddressCC = MDTConfigDBUtils.getEmailAddressesForCC(emailAddress)
  print "| INFO Sending summary e-mails to:", emailAddress
  print "| INFO with cc to:", emailAddressCC
  inputs = MDTConfigDBUtils.struct( 
                                    chamberName       = '',
                                    configSet         = configSetName, 
                                    deviceName        = deviceName,
                                    deviceConfig      = deviceConfigName,
                                    devicePin         = '',
                                    expectedDevicePin = '',
                                    parameterName     = '',
                                    deviceChannel     = '',
                                    currentValue      = '',
                                    wantedValue       = ''
                                  )
  hostName = os.getenv("HOSTNAME")
  if hostName == 'None':
    print "| ERROR Variable HOSTNAME not set!"
    print "| ERROR This is needed to determine the DB you need to read/write!"
    print "| ERROR Please set this variable or contact harper@cern.ch"
    sys.exit(2)
  elif 'pc-atlas-' in hostName: dbInstance = 'atonr_w'
  else: dbInstance = 'devdb11'
  status = outdateDeviceConfigSetItems(dbInstance, inputs)

