% --------------- %
% ---  JOB    --- %
% --------------- %

Job: "di_higgs_fitting_0618_unblind_test-10bins-nocut" 
  CmeLabel: "13 TeV"
  POI: "MuHH
  ReadFrom: NTUP
  NtuplePaths: "/lustre/samples/di-higgs/sample-applied-0526"
  Label: "non-resonant HH"
  LumiLabel: "139 fb^{-1}"    % data17?
  MCweight: "weight"
  Lumi: 139
  NtupleName: "ntup"
  DebugLevel: 2
  MCstatThreshold: NONE
  % MergeUnderOverFlow: TRUE
  SystControlPlots: TRUE
  SystErrorBars: TRUE
  HistoChecks: NOCRASH
  % StatOnly: TRUE
  SplitHistoFiles: TRUE   % set this in order to have separated .root files in Histograms/ for different regions
  ImageFormat: "png","pdf"
  DoSignalRegionsPlot: TRUE
  DoPieChartPlot: TRUE
  PlotOptions: NOXERR
  %PlotOptions: "NOXERR","OVERSIG","NOSIG"
  LegendNColumns: 1
  

% --------------- %
% ---  FIT    --- %
% --------------- %

Fit: "fit"
  FitRegion: CRONLY
  FitType: SPLUSB
  POIAsimov: 0
  FitBlind: FALSE
  doLHscan: "MuHH"
  NumCPU: 15

% --------------- %
% ---  LIMIT    --- %
% --------------- %

Limit: "limit"
  LimitType: ASYMPTOTIC
  LimitBlind: FALSE
  POIAsimov: 0

% -------------------- %
% --- Significance --- %
% -------------------- %
Significance: "significance"
  SignificanceBlind: FALSE
  POIAsimov: 1


% --------------- %
% --- REGIONS --- %
% --------------- %

Region: "ll_highm"
  Type: CONTROL
  Selection: "ll_m>110.0&&m_hh_truth==0"
  Variable: "ll_m",30,110,410
  VariableTitle: "m_{ll} [GeV]"
  Label: "t#bar{t} CR"
  BinWidth: 10
  LogScale: FALSE

Region: "DY_control"
  Type: CONTROL
  Selection: "is_em==0&&is_me==0&&ll_m>75&&ll_m<110&&m_hh_truth==0&&pytorch_region1_classifier2>0.6"
  Variable: "pytorch_region1_classifier2",10,0.6,1.0
  VariableTitle: "Drell-Yan Node NN Score"
  Label: "Drell-Yan CR"
  BinWidth: 0.04
  LogScale: TRUE
  

%Region: "DY_control"
% Type: CONTROL
 % Selection: "is_em==0&&is_me==0&&ll_m>75&&ll_m<110&&(bb_m<40||bb_m>210)"
 % Variable: "ll_m",10,75,110
 % VariableTitle: "m_{ll} [GeV]"
 % Label: "Drell-Yan CR"
 % BinWidth: 3.5
  %LogScale: TRUE



%Region: "DY_control_LM"
 % Type: CONTROL
 % Selection: "is_em==0&&is_me==0&&ll_m<75&&m_hh_truth==0&&pytorch_region3_classifier2>0.8"
 % Variable: "pytorch_region3_classifier2",10,0.8,1.0
 % VariableTitle: "Drell-Yan Node NN Score"
 % Label: "Drell-Yan CR"
 % BinWidth: 0.02
  %LogScale: TRUE  

  
Region: "NN_Region1"
  Type: SIGNAL
  %Selection: "is_em==0&&is_me==0&&ll_m>75&&ll_m<110&&bb_m>80&&bb_m<150&&bb_deltar<2.8&&m_hh_truth==0"
  Selection: "is_em==0&&is_me==0&&ll_m>75&&ll_m<110&&m_hh_truth==0"
  Variable: "pytorch_region1_classifier0",10,0,1
  VariableTitle: "signal node NN Score"
  Label: "HM/SF region"
  BinWidth: 0.1
  LogScale: TRUE

  
Region: "NN_Region2"
  Type: SIGNAL
  %Selection: "is_mm==0&&is_ee==0&&ll_m>75&&ll_m<110&&bb_m>80&&bb_m<150&&bb_deltar<2.8&&m_hh_truth==0"
  Selection: "is_mm==0&&is_ee==0&&ll_m>75&&ll_m<110&&m_hh_truth==0"
  Variable: "pytorch_region2_classifier0",10,0,1
  VariableTitle: "signal node NN Score"
  Label: "HM/DF region"
  BinWidth: 0.1
  LogScale: TRUE
  
Region: "NN_Region3"
  Type: SIGNAL
  %Selection: "is_em==0&&is_me==0&&ll_m<75&&bb_m>80&&bb_m<150&&bb_deltar<2.8&&m_hh_truth==0"
  Selection: "is_em==0&&is_me==0&&ll_m<75&&m_hh_truth==0"
  Variable: "pytorch_region3_classifier0",10,0,1
  VariableTitle: "signal node NN Score"
  Label: "LM/SF region"
  BinWidth: 0.1
  LogScale: TRUE
  
Region: "NN_Region4"
  Type: SIGNAL
  %Selection: "is_mm==0&&is_ee==0&&ll_m<75&&bb_m>80&&bb_m<150&&bb_deltar<2.8&&m_hh_truth==0"
  Selection: "is_mm==0&&is_ee==0&&ll_m<75&&m_hh_truth==0"
  Variable: "pytorch_region4_classifier0",10,0,1
  VariableTitle: "signal node NN Score"
  Label: "LM/DF region"
  BinWidth: 0.1
  LogScale: TRUE
  


  
  
Region: "NN_Region1_top"
  Type: VALIDATION
  Selection: "is_me==0&&is_em==0&&ll_m>75&&ll_m<110&&m_hh_truth==0&&bb_m>80&&bb_m<150&&bb_deltar<2.8&&pytorch_region1_classifier1>0.1"
  Variable: "pytorch_region1_classifier1",18,0.1,1
  VariableTitle: "top node NN Score"
  Label: "HM/SF Region"
  BinWidth: 0.05
  LogScale: TRUE 
  
Region: "NN_Region1_DY"
  Type: VALIDATION
  Selection: "is_me==0&&is_em==0&&ll_m>75&&ll_m<110&&m_hh_truth==0&&bb_m>80&&bb_m<150&&bb_deltar<2.8"
  Variable: "pytorch_region1_classifier2",20,0,1
  VariableTitle: "Drell-Yan node NN Score"
  Label: "HM/SF Region"
  BinWidth: 0.05
  LogScale: TRUE 

Region: "NN_Region2_top"
  Type: VALIDATION
    Selection: "is_mm==0&&is_ee==0&&ll_m>75&&ll_m<110&&m_hh_truth==0&&bb_m>80&&bb_m<150&&bb_deltar<2.8&&pytorch_region2_classifier1>0.1"
  Variable: "pytorch_region2_classifier1",18,0.1,1
  VariableTitle: "top node NN Score"
  Label: "HM/DF Region"
  BinWidth: 0.05
  LogScale: TRUE 
  
Region: "NN_Region2_DY"
  Type: VALIDATION
  Selection: "is_mm==0&&is_ee==0&&ll_m>75&&ll_m<110&&m_hh_truth==0&&bb_m>80&&bb_m<150&&bb_deltar<2.8"
  Variable: "pytorch_region2_classifier2",20,0,1
  VariableTitle: "Drell-Yan node NN Score"
  Label: "HM/DF Region"
  BinWidth: 0.05
  LogScale: TRUE 
  
  
Region: "NN_Region3_top"
  Type: VALIDATION
  Selection: "is_me==0&&is_em==0&&ll_m<75&&m_hh_truth==0&&bb_m>80&&bb_m<150&&bb_deltar<2.8&&pytorch_region3_classifier1>0.1"
  Variable: "pytorch_region3_classifier1",18,0.1,1
  VariableTitle: "top node NN Score"
  Label: "LM/SF Region"
  BinWidth: 0.05
  LogScale: TRUE 
  
Region: "NN_Region3_DY"
  Type: VALIDATION
  Selection: "is_me==0&&is_em==0&&ll_m<75&&m_hh_truth==0&&bb_m>80&&bb_m<150&&bb_m>80&&bb_m<150&&bb_deltar<2.8"
  Variable: "pytorch_region3_classifier2",20,0,1
  VariableTitle: "Drell-Yan node NN Score"
  Label: "LM/SF Region"
  BinWidth: 0.05
  LogScale: TRUE   

Region: "NN_Region4_top"
  Type: VALIDATION
  Selection: "is_mm==0&&is_ee==0&&ll_m<75&&m_hh_truth==0&&bb_m>80&&bb_m<150&&bb_deltar<2.8&&pytorch_region4_classifier1>0.1"
  Variable: "pytorch_region4_classifier1",18,0.1,1
  VariableTitle: "top node NN Score"
  Label: "HM/DF Region"
  BinWidth: 0.05
  LogScale: TRUE 
  
Region: "NN_Region4_DY"
  Type: VALIDATION
  Selection: "is_mm==0&&is_ee==0&&ll_m<75&&m_hh_truth==0&&bb_m>80&&bb_m<150&&bb_deltar<2.8"
  Variable: "pytorch_region4_classifier2",20,0,1
  VariableTitle: "Drell-Yan node NN Score"
  Label: "HM/DF Region"
  BinWidth: 0.05
  LogScale: TRUE 
  
  
  
Region: "bb_m_region3"
  Type: VALIDATION
  Selection: "is_me==0&&is_em==0&&ll_m<75&&m_hh_truth==0&&bb_m>80&&bb_m<150&&bb_deltar<2.8"
  Variable: "bb_m",20,80,150
  VariableTitle: "m_{bb} [GeV]"
  Label: "LM/SF Region"
  BinWidth: 3.5
  LogScale: FALSE
  
Region: "bb_m_region4"
  Type: VALIDATION
  Selection: "is_mm==0&&is_ee==0&&ll_m<75&&m_hh_truth==0&&bb_m>80&&bb_m<150&&bb_deltar<2.8"
  Variable: "bb_m",20,80,150
  VariableTitle: "m_{bb} [GeV]"
  Label: "LM/DF Region"
  BinWidth: 3.5
  LogScale: FALSE


% --------------- %
% --- SAMPLES --- %
% --------------- %

Sample: "Data"
   Type: DATA
   NtupleFile: "data"

Sample: "DiHiggs"
  Type: SIGNAL
  Title: "HH"
  FillColor: 3
  LineColorRGB: 0,0,0
  NtupleFile: "tree_sig"
  MCweight: 1.0/139.0

  
  
Sample: "ttbar"
  Type: BACKGROUND
  Title: "top"
  FillColorRGB: 7,162,171
  LineColorRGB: 0,0,0
  NtupleFile: "bkg_top"
  MCweight: 1.0/139.0


Sample: "DY"
  Type: BACKGROUND
  Title: "Z#\to ll"
  FillColorRGB: 244,244,111
  LineColorRGB: 0,0,0
  NtupleFile: "bkg_DY"
  MCweight: 1.0/139.0



Sample: "diboson"
  Type: BACKGROUND
  Title: "diboson"
  FillColorRGB: 69,196,139
  LineColorRGB: 0,0,0
  NtupleFile: "bkg_diboson"
  MCweight: 1.0/139.0  % k-factor 1 or 1.7?
  
Sample: "fake"
  Type: BACKGROUND
  Title: "fake"
  FillColorRGB: 94,104,122
  LineColorRGB: 0,0,0
  NtupleFile: "bkg_fakes"
  MCweight: 1.0/139.0  % k-factor 1 or 1.7?

  
Sample: "Higgs"
  Type: BACKGROUND
  Title: "Higgs"
  FillColorRGB: 244,121,66
  LineColorRGB: 244,121,66
  NtupleFile: "bkg_Higgs"
  MCweight: 1.0/139.0  % k-factor 1 or 1.7?
  


%Sample: "DD"
%  Type: BACKGROUND
%  Title: "DD"
%  FillColor: 434
%  LineColor: 434
%  NtupleFile: "tree_DD"
%  MCweight: 1.0
%  NormalizedByTheory: FALSE

% --------------- %
% - NORMFACTORS - %
% --------------- %

NormFactor: "MuHH"
   Title: "#mu (HH)"
   Nominal: 1
   Min: 0
   Max: 50
   Samples: DiHiggs
   
NormFactor: "MuDY"
   Title: "#mu (DY)"
   Nominal: 1
   Min: 0
   Max: 3
   Samples: DY
  
NormFactor: "Muttbar"
   Title: "#mu (ttbar)"
   Nominal: 1
   Min: 0
   Max: 3
   Samples: ttbar

% NormFactor: "muLumi"
%   Title: "Lumi Scale"
%   Min: 0
%   Max: 100
%   Nominal: 1
%   Constant: TRUE
%   Samples: all

% --------------- %
% - SYSTEMATICS - %
% --------------- %

Systematic: "LUMI"
  Title: "Luminosity"
  Type: OVERALL
  Samples: all
  OverallUp: 0.021
  OverallDown: -0.021
  Category: Lumi
  





