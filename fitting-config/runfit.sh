#/bin/bash
mass=$1
xs=$2
cp ../config/dihiggs_pDNN.config.bak ../config/dihiggs_pDNN_${mass}GeV.config

sed -i "s/MASS/$mass/g" ../config/dihiggs_pDNN_${mass}GeV.config
sed -i "s/SIGXSECTION/$xs/g" ../config/dihiggs_pDNN_${mass}GeV.config
trex-fitter n ../config/dihiggs_pDNN_${mass}GeV.config
trex-fitter d ../config/dihiggs_pDNN_${mass}GeV.config
trex-fitter w ../config/dihiggs_pDNN_${mass}GeV.config
trex-fitter f ../config/dihiggs_pDNN_${mass}GeV.config
trex-fitter p ../config/dihiggs_pDNN_${mass}GeV.config
trex-fitter l ../config/dihiggs_pDNN_${mass}GeV.config
